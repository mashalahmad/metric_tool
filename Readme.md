### About the Repository

This repository is the implementation of an independent Metric tool using the Spoon Java parser

### What is this repository for?

This repository alculates class-level metrics in Java projects by means of static analysis of source code
and no configuration of any dependent libraries.

Following Cohesion metrics can be computed 

* ClassCohesion(CC)
* CohesionAmongMethods(CAMC)
* LackOfCohesionOfMethod5(LCOM5)
* LowLevelDesignSimilarityBasedClassCohesion(LSCC)

### Metric tool contribution in Research

We used this tool to compute the metric and evolution of Java classes in the paper 
*Ahmad, Mashal, and Mel Ó. Cinnéide. "Impact of stack overflow code snippets on software cohesion: a preliminary study." In Proceedings of the 16th International Conference on Mining Software Repositories, pp. 250-254. IEEE Press, 2019.*
[Research paper](https://dl.acm.org/citation.cfm?id=3341929)


### How to use it?


You need at least Java 8 to be able to compile and run this tool.
Simply clone the project and run the *ComputerMetrics* file




