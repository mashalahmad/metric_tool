package Metric_implwithspoon.MetricsSpoon;


import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Metric_implwithspoon.Metrics.ClassCohesion;
import Metric_implwithspoon.Metrics.MetricNotDefinedForSourceException;
import spoon.Launcher;
import spoon.reflect.CtModel;


public class ClassCohesionTest extends ClassCohesion {

	Launcher launcher = new Launcher();	
	 @Before
	    public void setUp() throws IOException {
	
	    	
		launcher.addInputResource("src/test/resources/ReversePolishNotation.java"); 
		launcher.getEnvironment().setAutoImports(true); 
		launcher.getEnvironment().setNoClasspath(true); 
		
	 }
	
	 
	@Test
	public void testCC() throws MetricNotDefinedForSourceException {
		CtModel model=launcher.buildModel(); 
		assertEquals(0.5,calculateCC(model),0);
	}
	
	
	
}
