package Metric_implwithspoon.MetricsSpoon;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import Metric_implwithspoon.Metrics.CohesionAmongMethods;
import Metric_implwithspoon.Metrics.MetricNotDefinedForSourceException;
import spoon.Launcher;
import spoon.reflect.CtModel;

public class CohesionAmongMethodsTest extends CohesionAmongMethods{

	Launcher launcher = new Launcher();	
	 @Before
	    public void setUp() throws IOException {
	
	    	
		launcher.addInputResource("src/test/resources/ReversePolishNotation.java"); 
		launcher.getEnvironment().setAutoImports(true); 
		launcher.getEnvironment().setNoClasspath(true); 
		
	 }
	
	 
	 
	 @Test
		public void testCAMC() throws MetricNotDefinedForSourceException {
			CtModel model=launcher.buildModel(); 
			assertEquals(0.25,calculateCAMC(model),0);
		}
		
		
}
