/*
 *                     GNU GENERAL PUBLIC LICENSE
 *                        Version 3, 29 June 2007
 *
 *     "Pasjanse" - solitaire - card games for one person
 *     Copyright (C) {2016}  {Magdalena Wilska}
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *      any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *
 */

package cards;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 *  Represents one card.
 *      Issues:
 *         * Fragile: This loads each card image from a file, and has
 *           the file-naming conventions for the card built into it.
 *           To change to another set of card images, it's necessary
 *           to change this code.
 *         * Issue: Altho it's common to put minor rendering capabilites
 *           in a class (eg, toString), it does violate the separation
 *           of UI from model.  Does this class go too far in this direction?
 *             * It gets the images.
 *             * It keeps track of it's x,y coordinates.
 *             * It can draw itself.
 *
 * @author Fred Swartz - February  2007 - (for freecell solitere) Placed in public domain
 * @author magdalena - 30.09.16 - adapted to many difrent kind of solitares.
 */

///////////////////////////////////////////////////////////////////// Class Card
public class Card {
    
    //================================================================ constants
    public  static final int CARD_WIDTH;  // Initialized in static initilizer.
    public  static final int CARD_HEIGHT; // Initialized in static intializer..
    
    private static final String    IMAGE_PATH = "/cardimages/";
    private static final ImageIcon BACK_IMAGE;  // Image of back of card.
    
    private static final Class CLASS = Card.class;
    private static final String PACKAGE_NAME;
    private static final ClassLoader CLSLDR;
    
    //======================================================= static initializer
    static {
        //... Get current classloader, and get the image resources.
        //    This was broken down into small steps for debugging,
        //    They could easily be combined.
        PACKAGE_NAME = CLASS.getPackage().getName();
        CLSLDR = CLASS.getClassLoader();
        String urlPath = PACKAGE_NAME + IMAGE_PATH + "b.gif";
        java.net.URL imageURL = CLSLDR.getResource(urlPath);
        BACK_IMAGE = new ImageIcon(imageURL);
        
        //... These constants are assumed to work for all cards.
        CARD_WIDTH  = BACK_IMAGE.getIconWidth();
        CARD_HEIGHT = BACK_IMAGE.getIconHeight();
    }
    
    //======================================================= instance variables
    private Face _face;
    private Suit _suit;


    private ImageIcon _faceImage;
    private int     _x;
    private int     _y;
    private boolean _faceUp  = true;



    //============================================================== constructor

    public Card(Face face, Suit suit) {
        //... Set the face and suit values.
        _face = face;
        _suit = suit;
        
        //... Assume card is at 0,0
        _x = 0;
        _y = 0;
        
        //... By default the cards are face up.
        _faceUp = false;
        
        //... Read in the image associated with the card.
        //    Each card is stored in a GIF file where the name has two chars,
        //    eg, 3h.gif for the three of hearts.  A directory of these files
        //    is stored in the .jar file.
        
        //... Create the file name from the face and suit.
        char faceChar = "a23456789tjqk".charAt(_face.ordinal());
        char suitChar = "shcd".charAt(_suit.ordinal());
        String cardFilename = "" + faceChar + suitChar + ".gif";


        //... To get files from the current jar file, the class loader
        //    can be used.  I can return a URL of the card file.
        //... Get current classloader, and get the image resources.
        String path = PACKAGE_NAME + IMAGE_PATH + cardFilename;
        java.net.URL imageURL = CLSLDR.getResource(path);
        
        //... Load the image from the URL.
        _faceImage = new ImageIcon(imageURL);
    } //end of constructor

    //================================================================== setIcon
    /**
     * Set new Icon on the card face
     * (only need for jocker (probably there is better solution)
     * @param newIcon is a icon which you want to set on the card. For normal cards make a little sense changing icon, but ...
     */
    public void setIcon(ImageIcon newIcon) {_faceImage=newIcon;}

    //================================================================== getIcon
    /**
     * Get  Card's Icon from the card face
     * Now don't know if it is needed but ...
     * @return  faceImage in ImageIcon format  is a icon from card face?
     */
    public ImageIcon get_faceImage() { return _faceImage; }

    //***********************************************************************************************
    //=============================================================== turnFaceUp
    /**
     *  Turn card face up (so icon is seen)
     * _faceUp fiel special seter (for true value)
     */
    public void turnFaceUp() {_faceUp = true;}
    //============================================================= turnFaceDown
    /**
     *  Turn card face down  (so icon is  not possible to see)
     * _faceUp fiel special seter (for false value)
     */
    public void turnFaceDown() {_faceUp = false;}
    //============================================================= checkifFaceUp
    /**
     *  Check if  card is  face up or down
     * _faceUp fiel getter (boolean value)
     *  @return faceUp valure (boolean value) saying if card is vissible or not
     */
    public boolean is_faceUp() {return _faceUp; }

    //***********************************************************************************************
    //================================================================== getFace
    /**
     * Returns face value of card, which is one of the possible of ENUM Face:
     * ACE, DEUCE, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING.
     * */
    public Face getFace() {
        return _face;
    }
    //================================================================== getSuit
    /**
     * Returns suit of the card, which is one of the possible of ENUM Suit:
     * SPADES, HEARTS, CLUBS, DIAMONDS.
     * */
    public Suit getSuit() {
        return _suit;
    }
    
    //============================================================== setPosition
    /** Set Possition of the card (needed to draw)
     * */
    public void setPosition(int x, int y) {
        _x = x;
        _y = y;
    }
    //===================================================================== drawNegativ

    /**
     * Draws either face or back of the card.
     **/
    public void drawNegativ(Graphics g) {
        if (_faceUp) {
            ImageIcon _negativFaceImage = new ImageIcon();
            Image img = _faceImage.getImage();
            BufferedImage bimg = toBufferedImage(img);
            for (int x = 0; x < CARD_WIDTH; x++) {
                for (int y = 0; y < CARD_HEIGHT; y++) {


                    int rgba = bimg.getRGB(x, y);
                    Color col = new Color(rgba, true);
                    col = new Color(255 - col.getRed(),
                            255 - col.getGreen(),
                            255 - col.getBlue());
                    bimg.setRGB(x, y, col.getRGB());
                }
            }

            _negativFaceImage = new ImageIcon(bimg);

            _negativFaceImage.paintIcon(null, g, _x, _y);
        } else {
            BACK_IMAGE.paintIcon(null, g, _x, _y);
        }
    }

    /**
     * Converts a given Image into a BufferedImage from
     * http://stackoverflow.com/questions/13605248/java-converting-image-to-bufferedimage
     *
     * @param img The Image to be converted
     * @return The converted BufferedImage
     */
    public static BufferedImage toBufferedImage(Image img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;
    }

    //===================================================================== draw
    /**
     * Draws either face or back of the card.
     **/
    public void draw(Graphics g) {
        if (_faceUp) {
            _faceImage.paintIcon(null, g, _x, _y);
        } else {
            BACK_IMAGE.paintIcon(null, g, _x, _y);
        }
    }
    //================================================================= isInside

    /** Given a point, it tells whether this is inside card image.
    *   Used to determine if mouse was pressed in card.
    */
      public boolean isInside(int x, int y) {
        return (x >= _x && x < _x+CARD_WIDTH) && (y >= _y && y < _y+CARD_HEIGHT);
    }
    
    //=============================================================== getX, getY
    public int getX() {return _x;}
    public int getY() {return _y;}
    
    //=============================================================== getX, getY
    public void setX(int x) {_x = x;}
    public void setY(int y) {_y = y;}
    
    //================================================================= toString
    public String toString() {
        return "" + _face + " of " + _suit;
    }

}