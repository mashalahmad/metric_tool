/*
 * Copyright (C) 2014 Zhou_Rui
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.candy.gui;

import com.candy.common.GUITableRecord;
import com.candy.common.Pair;
import com.candy.controls.CategoryChart;
import com.candy.controls.DateNumberLineChart;
import com.candy.controls.FlexTable;
import com.candy.middle.AnalyzeProc;
import com.candy.middle.AnalyzeProc.SymbolData;
import com.candy.middle.FinReportDownload;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Zhou Rui <wirelesser at hotmail.com>
 */
public class AnalyzeTab extends Tab implements GenericTabIntf{
    private final String name = "Analyze";
    private final VBox root = new VBox();
    private final SplitPane splitPane = new SplitPane();
    private final ToolBar toolBar = new ToolBar();
    private final Button clrBtn = new Button("Clear Chart");    
    private final DateNumberLineChart lcChart = new DateNumberLineChart();
    private final CategoryChart cgChart = new CategoryChart();
    //private final ArrayList<String> colNameLst = new ArrayList();   // store column name start from 1,2,3..
    private final StackPane chartRoot = new StackPane();
    private final RadioButton yearBtn = new RadioButton("Annual");
    private final RadioButton quarterBtn = new RadioButton("Quarterly");    
    private final RadioButton incomeBtn = new RadioButton("Income");
    private final RadioButton balanceBtn = new RadioButton("Balance");
    private final RadioButton cashBtn = new RadioButton("Cash");
    private final TextField txtNumReport = new TextField("NumRec"); //  retrieve number of report
    private final Button downloadBtn = new Button("Download");
    private final ComboBox compareBox = new ComboBox();
    private final ToggleGroup chartGroup = new ToggleGroup();
    private final RadioButton dateChartBtn = new RadioButton("Date Chart");
    private final RadioButton qoqChartBtn = new RadioButton("QoQ Chart");
    private int finType = 0; // 0 - income ; 1 - balance; 2 - cash
    private boolean qtv = true; // false - annual, true - quarter time view
    private FlexTable table = null;
    private AnalyzeProc anaProc = null;
    
    public AnalyzeTab(AnalyzeProc proc) {
        this.anaProc = proc;
        anaProc.registerGui(this);
        table = new FlexTable(anaProc.getColumnName(), anaProc.getColumnEditableFlag());
        table.setRowStyle("highlightedRow");
        
        final ProgressBar progressBar = new ProgressBar(0);
         
        final ToggleGroup tvgroup = new ToggleGroup();
        yearBtn.setToggleGroup(tvgroup);
        quarterBtn.setToggleGroup(tvgroup);
        yearBtn.setUserData("annual");
        quarterBtn.setUserData("quarterly");quarterBtn.setSelected(true);
        tvgroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            public void changed(ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) {
                if (tvgroup.getSelectedToggle() != null) {
                    if (tvgroup.getSelectedToggle().getUserData().toString().equalsIgnoreCase("annual")) {
                        finType = 0;
                    } else {
                        finType = 1;
                    }
                }
            }
        });
        
        final ToggleGroup fintgroup = new ToggleGroup();
        incomeBtn.setToggleGroup(fintgroup);
        balanceBtn.setToggleGroup(fintgroup);
        cashBtn.setToggleGroup(fintgroup);        
        incomeBtn.setUserData("income");
        balanceBtn.setUserData("balance");
        cashBtn.setUserData("balance");
        incomeBtn.setSelected(true);
        fintgroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            public void changed(ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) {
                if (fintgroup.getSelectedToggle() != null) {
                    String value = fintgroup.getSelectedToggle().getUserData().toString();
                    if (value.equalsIgnoreCase("income")) {
                        finType = 0;
                    } else if (value.equalsIgnoreCase("cash")){
                        finType = 1;
                    } else if (value.equalsIgnoreCase("balance")){
                        finType = 2;
                    }
                }
            }
        });
        
        final ComboBox symbolBox = new ComboBox();
        symbolBox.setPromptText("stock symbol");
        symbolBox.setEditable(true);  
        symbolBox.setMaxHeight(Double.MAX_VALUE);
        symbolBox.setMaxWidth(Double.MAX_VALUE);
        symbolBox.setMinWidth(96);
        symbolBox.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
                // https://javafx-jira.kenai.com/browse/RT-29183 , use Control key temporarily
                if (ke.getCode().equals(KeyCode.CONTROL)) {
                    String symbol = symbolBox.getEditor().getText();                    
                    if (!symbol.isEmpty()) {
                        table.setTitle(symbol); 
                        lcChart.setTitle(symbol);
                        anaProc.loadFundamentalData(symbol, finType, qtv);
                    }
                }
            }
        });
        
        
        downloadBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent actionEvent) {
                final String symbol = symbolBox.getEditor().getText();                    
                if (symbol.isEmpty()) {
                    return;
                } 
                FinReportDownload finrDownload = new FinReportDownload();
                progressBar.progressProperty().unbind();
                progressBar.setProgress(0);
                progressBar.progressProperty().bind(finrDownload.progressProperty());                
                
                
                finrDownload.messageProperty().addListener(new ChangeListener<String>() {
                    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                        if (newValue.equalsIgnoreCase("DONE")) {
                            downloadBtn.setDisable(false);
                            table.setTitle(symbol); 
                            lcChart.setTitle(symbol);
                            anaProc.loadFundamentalData(symbol, finType, qtv);
                        }
                    }
                });                
                downloadBtn.setDisable(true);
                finrDownload.initSymbol(symbol);
                finrDownload.startTask();
            }
        });
        
        
        // move to new class?TODO
        compareBox.setPromptText("compare");
        compareBox.setEditable(false);  
        compareBox.setMaxHeight(Double.MAX_VALUE);
        compareBox.setMaxWidth(Double.MAX_VALUE);
        compareBox.setMinWidth(96);
        compareBox.getItems().addAll("Stock price", "S&P", "DJI");
        final ChangeListener cpboxListener = new ChangeListener<String>() {
            @Override 
            public void changed(ObservableValue ov, String t, String t1) {                
                if (t1==null)
                    return;
                addNewCompare(t1);
            } 
        };            
        compareBox.valueProperty().addListener(cpboxListener);
        
        clrBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent actionEvent) {
                lcChart.clearAllData();
                compareBox.getSelectionModel().clearSelection();
                compareBox.setValue(null);
            }
        });
        
        
        // progress bar
        toolBar.getItems().addAll(symbolBox, yearBtn, quarterBtn,incomeBtn, balanceBtn, cashBtn,
                txtNumReport,downloadBtn, compareBox, clrBtn,dateChartBtn,qoqChartBtn);
        progressBar.setId("narrow-progressbar");
        progressBar.prefWidthProperty().bind(root.widthProperty());
        
        // QoQ chart
        dateChartBtn.setToggleGroup(chartGroup);
        qoqChartBtn.setToggleGroup(chartGroup);
        dateChartBtn.setUserData("datechart");
        qoqChartBtn.setUserData("qoqchart");
        qoqChartBtn.setSelected(true);
        chartGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
            public void changed(ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) {
                if (chartGroup.getSelectedToggle() != null) {
                    // change time range
                    switchChart(chartGroup.getSelectedToggle().getUserData().toString());
                }
             }
        });
        switchChart("qoqchart");
        chartRoot.getChildren().addAll(lcChart.getGUI(), cgChart);
        root.getChildren().addAll(toolBar,progressBar, splitPane);
        VBox.setVgrow(splitPane, Priority.ALWAYS);        
        splitPane.getItems().addAll(table.getGUI(), chartRoot);
        splitPane.setOrientation(Orientation.VERTICAL); 
        // root.setStyle("-fx-border-color:red");
        
        table.getGUI().setOnMouseClicked(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() >1 ) {
                    handleRowDbClick();
                }
            }
        });
        setText(name);
        setContent(root);
        setClosable(false);
    }
    
    public ObservableList<Integer> getStyleRowLst() {
        return table.getStyledRowIndices();
    }
    
    public VBox getGUI() {
        return root;
    }
    
    private void switchChart(String sel) {
        if (sel.equalsIgnoreCase("datechart")) {
            this.lcChart.setVisible(true);
            this.cgChart.setVisible(false);
        } else if (sel.equalsIgnoreCase("qoqchart")) {
            this.lcChart.setVisible(false);
            this.cgChart.setVisible(true);
        }
    }
    /**
     * update the GUI data
     * @param recs 
     */
    public void refreshData(ObservableList<GUITableRecord> recs) {        
        table.reInitColumn(anaProc.getColumnName(), anaProc.getColumnEditableFlag());        
        table.refreshData(recs);        
    }
    
    /**
     * handle double click on row
     */
    private void handleRowDbClick() {
        ArrayList<String> oriColNameLst = anaProc.getColumnName();
        if (oriColNameLst!=null && oriColNameLst.size()>1) {
//            colNameLst.clear();
//            for (int i = oriColNameLst.size() - 1;i > 0 ;i--) {
//                colNameLst.add(oriColNameLst.get(i));
//            }
            GUITableRecord grec = (GUITableRecord)((TableView)table.getGUI()).getSelectionModel().getSelectedItem();
            String sel = chartGroup.getSelectedToggle().getUserData().toString();
            if (sel.equalsIgnoreCase("datechart")) {
                ArrayList<Double> colValueLst = new ArrayList();  // store column value
                for (int i = grec.getSize() - 1; i > 0 ; i--) {
                    colValueLst.add(grec.getDouble(i));
                }
                lcChart.addSeriesData(grec.getString(0), colValueLst, anaProc.getColumnAsDate());   
            } else if (sel.equalsIgnoreCase("qoqchart")) {
                ArrayList<Pair<Integer,Integer>> yearQtrLst = anaProc.getYearQuarterLst(); // table order
                LinkedHashMap<Integer, Double[]> catDataMaps = new LinkedHashMap();
                for (int i = grec.getSize() - 1; i > 0 ; i--) {
                    Pair<Integer,Integer> p = yearQtrLst.get(i-1); // column size has one more element
                    Integer year = p.first;
                    Integer quarter = p.second;
                    Double val = grec.getDouble(i);
                    Double[] valarray= catDataMaps.get(year);
                    if (valarray==null) {
                        valarray = new Double [] {0.0,0.0,0.0,0.0};
                        catDataMaps.put(year,valarray);
                    }
                    if (quarter <=4)
                        valarray[quarter-1] = val;
                }
                
                for (Integer year: catDataMaps.keySet()) {
                    Double[] valarray = catDataMaps.get(year);
                    if (valarray!=null) {
                        // http://stackoverflow.com/questions/5178854/convert-a-double-array-to-double-arraylist
                        // asList doesn't work with primitives
                        // http://stackoverflow.com/questions/4658867/why-does-arrays-aslist-return-its-own-arraylist-implementation
                        // It is because the Arrays$ArrayList returned by Arrays.asList is just a view on the original array. So when the original array is changed then the view is changed too.
                        String name = grec.getString(0) + String.valueOf(year);
                        cgChart.addSeriesData(name, new ArrayList<Double>(Arrays.asList(valarray)));
                    }
                }
                
            }
            
        }
    }
    
    
    // add symbol to chart
    private void addNewCompare(String symbol) {
        // retrieve symbol data
        if (symbol.equalsIgnoreCase("Stock price"))
            symbol = table.getTitle();        
        SymbolData sd = anaProc.loadSymbolPrice(symbol);
        if (sd!=null) {            
            // lcChart.setXaxisLabel(sd.getLabels());
            lcChart.addSeriesData(symbol, sd.getValues(), sd.getLabels());   
        }
    }
}
