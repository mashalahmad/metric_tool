/*
 *                                  Apache License
 *                            Version 2.0, January 2004
 *                         http://www.apache.org/licenses/
 *
 *    TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION
 *
 *    1. Definitions.
 *
 *       "License" shall mean the terms and conditions for use, reproduction,
 *       and distribution as defined by Sections 1 through 9 of this document.
 *
 *       "Licensor" shall mean the copyright owner or entity authorized by
 *       the copyright owner that is granting the License.
 *
 *       "Legal Entity" shall mean the union of the acting entity and all
 *       other entities that control, are controlled by, or are under common
 *       control with that entity. For the purposes of this definition,
 *       "control" means (i) the power, direct or indirect, to cause the
 *       direction or management of such entity, whether by contract or
 *       otherwise, or (ii) ownership of fifty percent (50%) or more of the
 *       outstanding shares, or (iii) beneficial ownership of such entity.
 *
 *       "You" (or "Your") shall mean an individual or Legal Entity
 *       exercising permissions granted by this License.
 *
 *       "Source" form shall mean the preferred form for making modifications,
 *       including but not limited to software source code, documentation
 *       source, and configuration files.
 *
 *       "Object" form shall mean any form resulting from mechanical
 *       transformation or translation of a Source form, including but
 *       not limited to compiled object code, generated documentation,
 *       and conversions to other media types.
 *
 *       "Work" shall mean the work of authorship, whether in Source or
 *       Object form, made available under the License, as indicated by a
 *       copyright notice that is included in or attached to the work
 *       (an example is provided in the Appendix below).
 *
 *       "Derivative Works" shall mean any work, whether in Source or Object
 *       form, that is based on (or derived from) the Work and for which the
 *       editorial revisions, annotations, elaborations, or other modifications
 *       represent, as a whole, an original work of authorship. For the purposes
 *       of this License, Derivative Works shall not include works that remain
 *       separable from, or merely link (or bind by name) to the interfaces of,
 *       the Work and Derivative Works thereof.
 *
 *       "Contribution" shall mean any work of authorship, including
 *       the original version of the Work and any modifications or additions
 *       to that Work or Derivative Works thereof, that is intentionally
 *       submitted to Licensor for inclusion in the Work by the copyright owner
 *       or by an individual or Legal Entity authorized to submit on behalf of
 *       the copyright owner. For the purposes of this definition, "submitted"
 *       means any form of electronic, verbal, or written communication sent
 *       to the Licensor or its representatives, including but not limited to
 *       communication on electronic mailing lists, source code control systems,
 *       and issue tracking systems that are managed by, or on behalf of, the
 *       Licensor for the purpose of discussing and improving the Work, but
 *       excluding communication that is conspicuously marked or otherwise
 *       designated in writing by the copyright owner as "Not a Contribution."
 *
 *       "Contributor" shall mean Licensor and any individual or Legal Entity
 *       on behalf of whom a Contribution has been received by Licensor and
 *       subsequently incorporated within the Work.
 *
 *    2. Grant of Copyright License. Subject to the terms and conditions of
 *       this License, each Contributor hereby grants to You a perpetual,
 *       worldwide, non-exclusive, no-charge, royalty-free, irrevocable
 *       copyright license to reproduce, prepare Derivative Works of,
 *       publicly display, publicly perform, sublicense, and distribute the
 *       Work and such Derivative Works in Source or Object form.
 *
 *    3. Grant of Patent License. Subject to the terms and conditions of
 *       this License, each Contributor hereby grants to You a perpetual,
 *       worldwide, non-exclusive, no-charge, royalty-free, irrevocable
 *       (except as stated in this section) patent license to make, have made,
 *       use, offer to sell, sell, import, and otherwise transfer the Work,
 *       where such license applies only to those patent claims licensable
 *       by such Contributor that are necessarily infringed by their
 *       Contribution(s) alone or by combination of their Contribution(s)
 *       with the Work to which such Contribution(s) was submitted. If You
 *       institute patent litigation against any entity (including a
 *       cross-claim or counterclaim in a lawsuit) alleging that the Work
 *       or a Contribution incorporated within the Work constitutes direct
 *       or contributory patent infringement, then any patent licenses
 *       granted to You under this License for that Work shall terminate
 *       as of the date such litigation is filed.
 *
 *    4. Redistribution. You may reproduce and distribute copies of the
 *       Work or Derivative Works thereof in any medium, with or without
 *       modifications, and in Source or Object form, provided that You
 *       meet the following conditions:
 *
 *       (a) You must give any other recipients of the Work or
 *           Derivative Works a copy of this License; and
 *
 *       (b) You must cause any modified files to carry prominent notices
 *           stating that You changed the files; and
 *
 *       (c) You must retain, in the Source form of any Derivative Works
 *           that You distribute, all copyright, patent, trademark, and
 *           attribution notices from the Source form of the Work,
 *           excluding those notices that do not pertain to any part of
 *           the Derivative Works; and
 *
 *       (d) If the Work includes a "NOTICE" text file as part of its
 *           distribution, then any Derivative Works that You distribute must
 *           include a readable copy of the attribution notices contained
 *           within such NOTICE file, excluding those notices that do not
 *           pertain to any part of the Derivative Works, in at least one
 *           of the following places: within a NOTICE text file distributed
 *           as part of the Derivative Works; within the Source form or
 *           documentation, if provided along with the Derivative Works; or,
 *           within a display generated by the Derivative Works, if and
 *           wherever such third-party notices normally appear. The contents
 *           of the NOTICE file are for informational purposes only and
 *           do not modify the License. You may add Your own attribution
 *           notices within Derivative Works that You distribute, alongside
 *           or as an addendum to the NOTICE text from the Work, provided
 *           that such additional attribution notices cannot be construed
 *           as modifying the License.
 *
 *       You may add Your own copyright statement to Your modifications and
 *       may provide additional or different license terms and conditions
 *       for use, reproduction, or distribution of Your modifications, or
 *       for any such Derivative Works as a whole, provided Your use,
 *       reproduction, and distribution of the Work otherwise complies with
 *       the conditions stated in this License.
 *
 *    5. Submission of Contributions. Unless You explicitly state otherwise,
 *       any Contribution intentionally submitted for inclusion in the Work
 *       by You to the Licensor shall be under the terms and conditions of
 *       this License, without any additional terms or conditions.
 *       Notwithstanding the above, nothing herein shall supersede or modify
 *       the terms of any separate license agreement you may have executed
 *       with Licensor regarding such Contributions.
 *
 *    6. Trademarks. This License does not grant permission to use the trade
 *       names, trademarks, service marks, or product names of the Licensor,
 *       except as required for reasonable and customary use in describing the
 *       origin of the Work and reproducing the content of the NOTICE file.
 *
 *    7. Disclaimer of Warranty. Unless required by applicable law or
 *       agreed to in writing, Licensor provides the Work (and each
 *       Contributor provides its Contributions) on an "AS IS" BASIS,
 *       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *       implied, including, without limitation, any warranties or conditions
 *       of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A
 *       PARTICULAR PURPOSE. You are solely responsible for determining the
 *       appropriateness of using or redistributing the Work and assume any
 *       risks associated with Your exercise of permissions under this License.
 *
 *    8. Limitation of Liability. In no event and under no legal theory,
 *       whether in tort (including negligence), contract, or otherwise,
 *       unless required by applicable law (such as deliberate and grossly
 *       negligent acts) or agreed to in writing, shall any Contributor be
 *       liable to You for damages, including any direct, indirect, special,
 *       incidental, or consequential damages of any character arising as a
 *       result of this License or out of the use or inability to use the
 *       Work (including but not limited to damages for loss of goodwill,
 *       work stoppage, computer failure or malfunction, or any and all
 *       other commercial damages or losses), even if such Contributor
 *       has been advised of the possibility of such damages.
 *
 *    9. Accepting Warranty or Additional Liability. While redistributing
 *       the Work or Derivative Works thereof, You may choose to offer,
 *       and charge a fee for, acceptance of support, warranty, indemnity,
 *       or other liability obligations and/or rights consistent with this
 *       License. However, in accepting such obligations, You may act only
 *       on Your own behalf and on Your sole responsibility, not on behalf
 *       of any other Contributor, and only if You agree to indemnify,
 *       defend, and hold each Contributor harmless for any liability
 *       incurred by, or claims asserted against, such Contributor by reason
 *       of your accepting any such warranty or additional liability.
 *
 *    END OF TERMS AND CONDITIONS
 *
 *    Copyright 2017 Henryk Timur Domagalski
 *
 */

package net.henryco.opalette.api.glES.render.graphics.shaders.textures.extend;

import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLES20;

import net.henryco.opalette.api.glES.camera.Camera2D;
import net.henryco.opalette.api.glES.render.graphics.shaders.textures.OPallTexture;

import java.nio.FloatBuffer;

/**
 * Created by HenryCo on 20/03/17.
 */

public class ConvolveTexture extends OPallTextureExtended {


	private static final String u_noiseLevel = "u_noiseLevel";
	private static final String u_matrixSize = "u_matrixSize";
	private static final String u_matrix3 = "u_matrix3";
	private static final String u_matrix5 = "u_matrix5";
	private static final String u_texDim = "u_screenDim";
	private static final String u_enable = "u_enable";

	public static final FilterMatrices matrix = new FilterMatrices();

	private float[] original_filter_matrix;
	private float[] work_filter_matrix;
	private float center_scale = 1;
	private float noiseLevel = 0;
	private int matrix_size = 0;
	private int matrix_sqrt_size = 0;
	private int scale_pow;

	private boolean enable = true;

	public ConvolveTexture(Context context) {
		this(context, Filter.LINEAR);
	}
	public ConvolveTexture(Context context, Filter filter) {
		super(context, filter, OPallTexture.DEF_SHADER+".vert", FRAG_DIR);
		init();
	}
	public ConvolveTexture(Bitmap image, Context context) {
		this(image, context, Filter.LINEAR);
	}
	public ConvolveTexture(Bitmap image, Context context, Filter filter) {
		super(image, context, filter, OPallTexture.DEF_SHADER+".vert", FRAG_DIR);
		init();
	}
	public ConvolveTexture() {
		super(OPallTexture.DEFAULT_VERT_FILE, FRAG_FILE);
		init();
	}
	public ConvolveTexture(Bitmap image) {
		this(image, Filter.LINEAR);
	}
	public ConvolveTexture(Bitmap image, Filter filter) {
		super(image, filter, OPallTexture.DEFAULT_VERT_FILE, FRAG_FILE);
		init();
	}


	private void init() {
		setFilterMatrix(matrix.m_identity());
		setScalePower(2);
	}

	@Override
	protected void render(int program, Camera2D camera) {
		GLES20.glUniform2f(GLES20.glGetUniformLocation(program, u_texDim), getWidth(), getHeight());
		GLES20.glUniform1f(GLES20.glGetUniformLocation(program, u_matrixSize), matrix_sqrt_size);
		GLES20.glUniform1f(GLES20.glGetUniformLocation(program, u_noiseLevel), noiseLevel);
		GLES20.glUniform1i(GLES20.glGetUniformLocation(program, u_enable), enable ? 1 : 0);
		String matrixTarget;
		if (matrix_sqrt_size == 3) matrixTarget = u_matrix3;
		else matrixTarget = u_matrix5;
		GLES20.glUniform1fv(GLES20.glGetUniformLocation(program, matrixTarget), matrix_size, FloatBuffer.wrap(work_filter_matrix));
	}




	public ConvolveTexture setFilterMatrix(float ... matrix) {

		if (matrix.length == 0 || (matrix.length == 1 && matrix[0] == -1))
			matrix = ConvolveTexture.matrix.m_identity();
		if (Math.sqrt(matrix.length) % 2 == 0)
			throw new RuntimeException(getClass().getName()
					+ ": Filter matrix dimension must be 3x3, 5x5, 7x7, 9x9 ...");
		original_filter_matrix = matrix;
		matrix_size = original_filter_matrix.length;
		matrix_sqrt_size = (int) Math.sqrt(matrix_size);
		return setEffectScale(center_scale);
	}

	public ConvolveTexture setEffectScale(final float s) {

		float sc = (float) Math.min(1, Math.pow(s, scale_pow));
		float[] matrix = new float[matrix_size];
		int center = (int) (0.5f * (matrix_sqrt_size - 1f));
		int cp = (center * matrix_sqrt_size) + center;
		for (int i = 0; i < matrix_size; i++) {
			matrix[i] = original_filter_matrix[i];
			if (i != cp) matrix[i] *= sc;
			else {
				float x = matrix[i];
				if (x != 0) {
					float rx = 1f / x;
					matrix[i] = rx + ((x - rx) * sc);
				} else matrix[i] = 1 - sc;
			}
		}
		work_filter_matrix = normalize(matrix);
		center_scale = s;
		return this;
	}

	public ConvolveTexture setScalePower(int scale_pow) {
		this.scale_pow = scale_pow;
		return this;
	}

	public float getEffectScale() {
		return center_scale;
	}


	private static float[] normalize(float[] matrix) {

		float sum = 0;
		for (float v : matrix) sum += v;
		if (sum == 0) return matrix;
		for (int i = 0; i < matrix.length; i++)
			matrix[i] /= sum;
		return matrix;
	}

	public ConvolveTexture setNoiseLevel(float noiseLevel) {
		this.noiseLevel = noiseLevel;
		return this;
	}

	public float getNoiseLevel() {
		return noiseLevel;
	}



	public ConvolveTexture setFilterEnable(boolean b) {
		this.enable = b;
		return this;
	}
	public boolean isFilterEnable() {
		return enable;
	}

	private static final String FRAG_DIR = OPallTexture.FRAG_DIR+"/ConvolveFilter.frag";
	private static final String FRAG_FILE = "precision highp float;\n" +
			"// necessary part\n" +
			"varying vec4 v_Position;\n" +
			"varying vec4 v_WorldPos;\n" +
			"varying vec2 v_TexCoordinate;\n" +
			"\n" +
			"uniform sampler2D u_Texture0;\n" +
			"\n" +
			"\n" +
			"// custom part\n" +
			"uniform float u_matrixSize;  // 3, 5\n" +
			"uniform float u_matrix3[9];\n" +
			"uniform float u_matrix5[25];\n" +
			"uniform float u_noiseLevel;\n" +
			"uniform vec2 u_screenDim;\n" +
			"uniform int u_enable; // 0 == false, 1... == true\n" +
			"\n" +
			"\n" +
			"float noise1f(vec2 co){\n" +
			"    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);\n" +
			"}\n" +
			"//http://stackoverflow.com/questions/12964279/whats-the-origin-of-this-glsl-rand-one-liner\n" +
			"vec3 noise3f(vec3 color) {\n" +
			"    if (u_noiseLevel > 0.01) {\n" +
			"		 float l = length(v_TexCoordinate);"+
			"        float r = noise1f(vec2(color.r, l));\n" +
			"        float g = noise1f(vec2(color.g, l));\n" +
			"        float b = noise1f(vec2(color.b, l));\n" +
			"        return mix(normalize(vec3(r, g, b)), color, vec3(1. - u_noiseLevel));\n" +
			"    }\n" +
			"    return color;\n" +
			"}\n" +
			"\n" +
			"void main() {\n" +
			"\n" +
			"    if (u_enable == 0) gl_FragColor = texture2D(u_Texture0, v_TexCoordinate).rgba;\n" +
			"    else {\n" +
			"        vec2 cor = vec2((u_matrixSize - 1.) / 2.);\n" +
			"        vec3 rgb = vec3(0.);\n" +
			"\n" +
			"        for (float i = 0.; i < u_matrixSize; i++) {\n" +
			"            for (float k = 0.; k < u_matrixSize; k++) {\n" +
			"                vec2 ipos = vec2(i - cor.x, k - cor.y);\n" +
			"                vec3 irgb = texture2D(u_Texture0, v_TexCoordinate + (ipos / u_screenDim)).rgb;\n" +
			"\n" +
			"                int n = int(i * u_matrixSize + k);\n" +
			"                if (u_matrixSize == 3.) irgb *= u_matrix3[n];\n" +
			"                else irgb *= u_matrix5[n];\n" +
			"\n" +
			"                rgb += irgb;\n" +
			"            }\n" +
			"        }\n" +
			"        gl_FragColor = vec4(noise3f(rgb), 1.);\n" +
			"    }\n" +
			"}";


	public static final class FilterMatrices {

		public final float[] m_identity(){
			return new float[] {
					0, 0, 0,
					0, 1, 0,
					0, 0, 0
			};
		}
		public final float[] m_boxBlur() {
			return new float[] {
					1, 1, 1,
					1, 1, 1,
					1, 1, 1
			};
		}
		public final float[] m_blur() {
			return new float[] {
					0, 0, 1, 0, 0,
					0, 1, 1, 1, 0,
					1, 1, 1, 1, 1,
					0, 1, 1, 1, 0,
					0, 0, 1, 0, 0
			};
		}
		public final float[] m_gaussianBlur() {
			return new float[] {
					1, 2, 1,
					2, 4, 2,
					1, 2, 1
			};
		}
		public final float[] m_sharpen() {
			return new float[] {
					-1, -1, -1, -1, -1,
					-1,  2,  2,  2, -1,
					-1,  2,  8,  2, -1,
					-1,  2,  2,  2, -1,
					-1, -1, -1, -1, -1
			};
		}
		public final float[] m_sharpen1() {
			return new float[] {
					0, -1, 0,
					-1, 5, -1,
					0, -1, 0
			};
		}
		public final float[] m_sharpen2() {
			return new float[] {
					-1, -1, -1,
					-1, 0, -1,
					-1, -1, -1
			};
		}
		public final float[] m_sharpen3() {
			return new float[]{
					-1, -1, -1,
					-1, 8, -1,
					-1, -1, -1
			};
		}
		public final float[] m_sharpen4() {
			return new float[]{
					1, -2, 1,
					-2, 5, -2,
					1, -2, 1
			};
		}
		public final float[] m_sharpen5() {
			return new float[] {
					-1, -1, -1, -1, -1,
					-1, 3, 4, 3, -1,
					-1, 4, 13, 4, -1,
					-1, 3, 4, 3, -1,
					-1, -1, -1, -1, -1
			};
		}
		public final float[] m_emboss1() {
			return new float[] {
					-2, -1, 0,
					-1, 1, 1,
					0, 1, 2
			};
		}
		public final float[] m_emboss2() {
			return new float[] {
					-2, 0, 0,
					0, 1, 0,
					0, 0, 2
			};
		}
		public final float[] m_diagShatter() {
			return new float[] {
					1, 0, 0, 0, 1,
					0, 0, 0, 0, 0,
					0, 0, 0, 0, 0,
					0, 0, 0, 0, 0,
					1, 0, 0, 0, 1
			};
		}
		public final float[] m_horizontalMotionBlur() {
			return new float[] {
					0, 0, 0, 0, 0,
					0, 0, 0, 0, 0,
					2, 3, 4, 5, 6,
					0, 0, 0, 0, 0,
					0, 0, 0, 0, 0
			};
		}
		public final float[] m_unsharp() {
			return new float[] {
					1, 4, 6, 4, 1,
					4, 16, 24, 16, 4,
					6, 24, -476, 24, 6,
					4, 16, 24, 16, 4,
					1, 4, 6, 4, 1
			};
		}

	}
}
