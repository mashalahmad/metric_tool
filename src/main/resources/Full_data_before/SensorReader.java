package shrine.sensorreader;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Get Sensor Events, extract critical information, format it (e.g. for easier gnuplot parsing or
 * efficient wire footprint) and write to output stream
 */
public class SensorReader implements SensorEventListener {
    protected static final String fileName = "output.csv";
    /**
     * dataSize defines the size of the newData buffer
     */
    private final int dataSize = 256;
    /**
     * A buffer for raw sensor data. If the buffer is full, its content is written to an output
     * file and the buffer gets overwritten.
     */
    protected float[] newData;
    /** dataCount counts the amount of data in the newData buffer. */
    private int dataCount;
    /**
     * sampleCounter is counting the overall amount of raw sensor samples. It is needed to plot the
     * data with GNUPlot
     */
    private int sampleCounter;
    /**
     * Output stream where the formatted data is being sent to
     */
    private OutputStreamWriter writer;

    public SensorReader(OutputStreamWriter w) throws IOException {
        newData = new float[3 * dataSize]; // room for dataSize x,y,z tuples
        dataCount = 0;
        sampleCounter = 0;
        writer = w;
        // Write the file header
        writer.write("# sample" + SensorReader.padRight("| x", 14)
                + SensorReader.padRight("| y", 16) + SensorReader.padRight("| z", 16) + "\n"
                + "#-----------------------------------------------------\n");
    }

    /* Source for this function: http://www.rgagnon.com/javadetails/java-0448.html
     */
    public static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);
    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Nothing to do here
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        newData[3 * dataCount] = event.values[0];
        newData[3 * dataCount + 1] = event.values[1];
        newData[3 * dataCount + 2] = event.values[2];
        dataCount++;
        /* if dataCount >= dataSize it means that the newData buffer is full.  In this case its
         * content gets written into the output file.
         */
        if (dataCount >= dataSize) {
            Log.d(MainActivity.TAG, "dataCount = " + dataCount + " >= " + dataSize + " = dataSize");
            try {
                writer.write(dataToString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            dataCount = 0;
        }
    }

    private String dataToString() {
        String result = "";
        for (int i = 0; i < dataCount; i++) {
            result += padRight(sampleCounter++ + ", ", 8) + padRight("" + newData[3 * i], 14)
                    + padRight(", " + newData[3 * i + 1], 16)
                    + padRight(", " + newData[3 * i + 2], 16) + "\n";
        }
        return result;
    }

    protected void saveRemainingData() throws IOException {
        writer.write(dataToString());
    }
}
