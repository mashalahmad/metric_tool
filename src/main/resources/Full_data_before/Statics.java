/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

/**
 *
 * @author shotbygun
 */
public class Statics {

    // Global Init Constants
    public static final String applicationName = "Hefty Frame Analysis";
    public static final String applicationShortName = applicationName.replace(" ", "");
    public static final String version = "v0.1";
    public static boolean debug = true;
    public static boolean dumpData = false;


    // Runtime variables

    public static String ffprobeExecutablePath = "D:\\Ohjelmat\\ffprobe\\bin\\ffprobe.exe";
}