package Metric_implwithspoon.Metrics;

import java.util.ArrayList;
import java.util.List;

import Metric_implwithspoon.Metrics.util.CohesionUtility;
import Metric_implwithspoon.Metrics.MetricNotDefinedForSourceException;
import spoon.reflect.CtModel;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.reference.CtTypeReference;

public class CohesionAmongMethods extends CohesionUtility  {
	public static double calculateCAMC( CtModel model ) throws MetricNotDefinedForSourceException {
		
		List<CtTypeReference<?>> parameterset = new ArrayList<CtTypeReference<?>>();
        double summation = 0.0;
        double result=0.0;
        List<CtMethod<?>> getmethods=getMethods(model);
        parameterset = getParameterSet(getmethods);
        int l = parameterset.size();
        int k = getmethods.size();

        /** If k = 0, the cohesion is not defined. */
        /**
         * If l = 0, the cohesion is not defined: CAMC is not defined for a
         * class where all methods are non-parametric.
         */
        if (k > 0 && l > 0) {
            int[][] parameterOcuranceMatrix = getParameterOccuranceMatrix(getmethods, parameterset);
            for (int i = 0; i < k; i++)
                for (int j = 0; j < l; j++)
                    summation += parameterOcuranceMatrix[i][j];
           
            result= summation / (k * l);
            return result;
            
        } else
        //throw new MetricNotDefinedForSourceException();
        {
        	return result= 0.0;
        	
        }
        
		
	}
	
	

}
