package Metric_implwithspoon.Metrics.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import spoon.reflect.CtModel;
import spoon.reflect.code.CtInvocation;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtConstructor;
import spoon.reflect.declaration.CtExecutable;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtParameter;
import spoon.reflect.declaration.CtType;
import spoon.reflect.reference.CtExecutableReference;
//import spoon.reflect.declaration.CtType;
import spoon.reflect.reference.CtFieldReference;
import spoon.reflect.reference.CtTypeReference;
import spoon.reflect.visitor.filter.OverriddenMethodFilter;
import spoon.reflect.visitor.filter.TypeFilter;

public class CohesionUtility extends MetricUtility  {
	
	public static List<CtMethod<?>> getMethods(CtModel model){
	 return model.getElements(new TypeFilter<CtMethod<?>>(CtMethod.class));	
	}
	
	public static List<CtField<?>> getFields(CtModel model){
		return model.getElements(new TypeFilter<CtField<?>>(CtField.class));
	}

	protected static List<CtMethod<?>>  GetMethodsnConstructor(CtModel model){
		 
		  List<CtMethod<?>> Allconstructornmethods = new ArrayList<CtMethod<?>>();
		
		  //List<CtConstructor<?>> constructors=  model.getElements(new TypeFilter<CtConstructor<?>>(CtConstructor.class));
		  
		  List <CtExecutable<?>> exeref=  model.getElements(new TypeFilter<CtExecutable<?>>(CtExecutable.class));
	      for(CtExecutable<?> exe: exeref){ 
	    	 // CtExecutable<?> executable = exe;
	    	  //tMethod<?> method = (CtMethod<?>) executable;
	    	  if((exe instanceof CtMethod)&& (!(exe.isImplicit())))
	    		  
	    	 // System.out.println(exe);
	    	  Allconstructornmethods.add((CtMethod<?>) exe);
	    	  
	      }
		  
		  
	 /*Allconstructornmethods =  model.getElements(new TypeFilter<CtMethod<?>>(CtMethod.class));
		 // CtMethod<?> method1=Allconstructornmethods.get(0);
		  //			CtType<?> declaringtype	=method1.getDeclaringType();
		  for(CtConstructor<?> constructor: constructors)
		  {
			  if(!(constructor.isImplicit())) {
				  
				  Allconstructornmethods.add((CtMethod<?>)constructor);
			  }
		  } */
	      return Allconstructornmethods;
	}
	
	
	
	
	
	
	/*/**********************************************************************************************************
	* Description : getAttributeReferences() 
	*               AR(m): for each m belongs to M(c) let AR(m) be the set of attributes referenced by method m.
	*           
	*               This methods returns all attribute which are defined in class cd and are referenced by "method", 
	*               it includes inheritance attributes.
	*               Based on "A Unified Framework for Cohesion Measurement in Object-Oriented Systems" page 71
	*  
	* Input: Method
	* Output: AR(m), it doesn't contain duplicate value
	*************************************************************************************************************/ 	
	
   	
static List<CtFieldReference<?>> getAttributeReferences(CtMethod<?> method){
        	//get the type which declares method
        	CtType<?> declaringType = method.getDeclaringType();
        	return method
        		//visit AST of the method and pass all the nodes of type CtFieldReference to the next query step
        		.filterChildren(new TypeFilter<>(CtFieldReference.class))
        		//.select((CtFieldReference<?> fieldRef)->fieldRef!=null && fieldRef.getDeclaringType().getQualifiedName().equals(declaringType.getQualifiedName()))
        		.list(); 
        	
        
    	}
        
    
    	public static Set<CtField<?>> getImplementAttributeReferences(CtMethod<?> method, CtClass<?> cd){
    		
    		Set<CtField<?>> ImplementAttributeReferences = new HashSet<CtField<?>>();
    		List<CtFieldReference<?>> fieldrefs = getAttributeReferences(method);
    		for (CtFieldReference<?> field : fieldrefs) 
    			  if (field.getDeclaringType()!=null&& field.getDeclaringType().toString().equals(cd.getQualifiedName())) 
    				ImplementAttributeReferences.add(field.getFieldDeclaration());
    		return ImplementAttributeReferences;
    	}
	
	
    
        /*/**********************************************************************************************************
         * Description : getMethodAttributeReferenceMatrix()
         *               The Method Attribute Reference Matrix (MAR) has the same rows and columns as "DirectAttributeMatrix", 
         *               but it differs in that it models both direct and transitive interactions.A value of 1 in the matrix 
         *               indicates that the filed (attribute) is accessed by the method or the method directly or transitively invoked 
         *               another the method that field is accessed there.
                         The Method-Attribute Reference (MAR) matrix is a binary "kï¿½l" matrix, where "k" is the number of methods and 
                         "l" is the number of attributes in the class of interest. 
         *               The MAR matrix has rows indexed by the methods and columns indexed by the attributes. 
         *               MAR(i,j) =1 if the "ith" method reference "jth" attribute. Otherwise, it is zero.
         *               The matrix implicitly models method-method interaction.
         *               A cohesive method-method interaction is represented in the MAR matrix by two rows that share 
         *               binary values of "1" in a column.
         *   
         * Input: List<CtMethod>,List<CtField>
         * Output:returns Method-Method Invocation Cohesion (MMIC)value. 
        ************************************************************************************************************/	
    	
            
    public static int[][] getMethodAttributeReferenceMatrix(List<CtMethod<?>> Methodlist,List<CtField<?>> Fieldlist,CtModel model ){
        
                int[][]  DirectAttributeMatrix = getDirectAttributeMatrix(Methodlist,Fieldlist);
                int[][]  MetodInvocationMatrix = getMethodInnovacationMatrix(Methodlist,model);
                int row = Methodlist.size();
                int[][]  MethodAttributeReferenceMatrix = getDirectAttributeMatrix(Methodlist,Fieldlist);
                
                for (int i = 0; i < row; i++) 
                    for (int j = 0; j < row; j++) 
                         if(MetodInvocationMatrix[i][j] == 1)
                            for (int k = 0; k < MethodAttributeReferenceMatrix[i].length; k++) 
                                if (DirectAttributeMatrix[j][k] == 1)
                                    MethodAttributeReferenceMatrix[i][k] = 1;      					
                return MethodAttributeReferenceMatrix;
            }

    /*/**********************************************************************************************************
     * Description : getDirectAttributeMatrix() 
     *               The Direct Attribute Matrix is a binary "k*l" matrix, where "k" is the number of 
     *               methods and "l" is the number of "attributes" in the class of interest.
     *               DA[i,j]=1 if ith method directly references jth attribute. 
     *               class one{ int x; foo(){ x=1}} "foo()" directly references "x". 
     *               
     *               Excluding or including inherited methods as well as attributes depends on algorithm. 
     *               Methodlist and Fieldlist is passed as methods's parameter.So, depends on algorithm it contains or no contains inherited elements.
     *               Exclude constructors                
     * Input: List<CtMethod>, List<CtField>
     * Output:return Direct Attribute (DA) Matrix. 
    ************************************************************************************************************/   

             
     private static int[][] getDirectAttributeMatrix(List<CtMethod<?>> Methodlist,List<CtField<?>> Fieldlist ){
                                        
                    int[][] DirectAttributeMatrix = new int[Methodlist.size()][Fieldlist.size()];            
                    for (int i = 0; i < Methodlist.size(); i++) 
                    	for (CtFieldReference<?> field : getAttributeReferences(Methodlist.get(i)))    	
    	                	if (Fieldlist.contains(field.getDeclaration())) 			                		
    			                  DirectAttributeMatrix[i][Fieldlist.indexOf(field.getDeclaration())] = 1;
                    return DirectAttributeMatrix;
                }        
     /*/**********************************************************************************************************
      * Description : getMethodInnovacationMatrix() 
      *               The Method Invocation Matrix is a square binary k*k matrix, where k is the number of methods in the 
      *               class of interest. The matrix models the "direct" and "transitive" method-invocation interaction and 
      *               is derived from DMI matrix.
      *               Based on "An object-oriented high-level design-base class cohesion metric page 1350" 
      *                               
      * Input: List<Method>
      * Output:return Method Invocation (MI) matrix. 
     ************************************************************************************************************/          
            
               
     private static int[][] getMethodInnovacationMatrix(List<CtMethod<?>> Methodlist,CtModel model){
                                
                        boolean flag = true;
                        int[][] MetodInvocationMatrix=getDirectMethodInnovacationMatrix(Methodlist,model);    
                
                        while (flag){
                            flag=false;
                            for (int i = 0; i < Methodlist.size(); i++)
                                for (int j = 0; j < Methodlist.size(); j++) 
                                	if ((MetodInvocationMatrix[i][j]==1)&&(i!=j))
                                        for (int k = 0; k < Methodlist.size(); k++) 
                                            if (MetodInvocationMatrix[j][k]==1)
                                                if (MetodInvocationMatrix[i][k]!=1){
                                                    MetodInvocationMatrix[i][k]=1;
                                                    flag=true; 
                                                }
                            							
                        }
    return MetodInvocationMatrix;
                    }
     /*/**********************************************************************************************************
      * Description : getDirectMethodInnovacationMatrix() 
      *               This matrix is a square binary k*k matrix, where k is the number of methods in the class of interest.
      *               m(i,j)=1 if the ith method invokes directly the jth method. otherwise, m(i,j)=0
      *              
      *               Including or excluding inherited methods depends on algorithm. 
      *               Methodlist is passed as method's parameter. So, depends on algorithm it contains or no contains inherited methods.  
      *               
      *               Exclude Constructors
      *               
      *               This method doesn't return real direct method invocation matrix, and its reason is dynamic binding.
      *               class One{ 
      *                  foo(){ One ob; if (i==1) ob=new One; else ob=new Two; ob.share;} 
      *                  Share{Print("One")}
      *               }
      *               class Two extends One{
      *                  share{Print("Two")}
      *               }
      *               Two two_ob=new Two(); two_on.foo(); => which share will be called??
      *               There are two ways: 1. implement getDirectMethodInnovacationMatrix like comment file.
      *               2. suppose a situation like this is rare and implement method as below.
      *               both 1, and 2 ways have some problems, but because it seems this situation is so rare second
      *               strategy is selected.  
      * Input: List<CtMethod>
      * Output:return Direct Method Invocation (DMI) matrix. 
     ************************************************************************************************************/   
     
     private static  int[][] getDirectMethodInnovacationMatrix(List<CtMethod<?>> Methodlist,CtModel model){

         int[][] DirectMetodInvocationMatrix=new int[Methodlist.size()][Methodlist.size()];
         
         for (int i = 0; i < Methodlist.size(); i++)
         { 
        	 for (CtMethod<?> invokedMethod : staticallyInvokedMethods(Methodlist.get(i))) 
             {    
             	int index=Methodlist.indexOf(invokedMethod);
         		if(index==-1){
         				List<CtMethod<?>> RedifiningMethods=model.getElements(new OverriddenMethodFilter(invokedMethod));
         				for (CtMethod<?> method : RedifiningMethods) {
                            index=Methodlist.indexOf(method);
                            if(index!=-1) break;
                        }
         				}
         				
             	if(index!=-1) 
                DirectMetodInvocationMatrix[i][index]=1;
               
             }
         }
         return DirectMetodInvocationMatrix;
     }  
     
       /******* Metric Utility   ********/            
     /*/**********************************************************************************************************
      * Description : staticallyInvokedMethods() --- SIM(m)
      *               Let "c1" belongs to "C", and "m" belongs to MI(c) and "m'" belongs to M(C). 
      *               Then "m'" belongs to SIM(m) if and only if exist a "c2" belongs to "C" such
      *               that "m'" belongs to M(c2) and the body of "m" has a method invocation where
      *               "m'" is invoked for an object of static type class "c2".
      *               
      *               Based on "A Unified Framework for Cohesion Measurement in Object-Oriented Systems" page 70
      *              
      *               In a situation like below, the method returns superclass foo() method. 
      *                  class Four{ One ob; if (i == 1) ob = new Two(); else ob = new Three();}             
      *                  where   
      *                  class One{ public void foo(){..}}
      *                  class Two extends One{public void foo(){..}}
      *                  class Three extends One{public void foo(){..}}   
      *              
      *              I do'nt count method which its class is an Interface. They will bind to a real
      *              class at run time(Dynamic binding),so we don't need them in this function.              
      *
      * Input:  Method
      * Output: List<CMethod>
     *************************************************************************************************************/                     

    private static   List<CtMethod<?>> staticallyInvokedMethods(CtMethod<?> method){
    		   
    	       List<CtMethod<?>> staticallyInvokedMethodSet =new ArrayList<CtMethod<?>>();
    	       CtType<?> declaringType = method.getDeclaringType();
    		  
    	       List <CtExecutableReference<?>> exeref= method.filterChildren(new TypeFilter<CtInvocation<?>>(CtInvocation.class)).map((CtInvocation<?> inv) -> inv.getExecutable()).list();
    	      for(CtExecutableReference<?> exe: exeref){ 
    	    	  CtExecutable<?> executable = exe.getDeclaration();
    	    	  CtMethod<?> Invokedmethod = (CtMethod<?>) executable;
    	    	  if((Invokedmethod!=null)&&(!method.equals(Invokedmethod))&&(Invokedmethod.getDeclaringType().getQualifiedName().equals(declaringType.getQualifiedName())))
    	    		  staticallyInvokedMethodSet.add(Invokedmethod);
    	       }
    	      
    return staticallyInvokedMethodSet;	       
     }
	
    /*/**********************************************************************************************************
	 * Description : getCommonAttributeUsageMatrix()
	 * 				 The matrix is k*k where k is the number of methods which pass as input.
	 * 				 A[i,j]=1 if cau(i,j)==1. In fact, each room contains cau for that array room.				 
	 * 				 cau(m1,m2)(common attribute usage) is true, if m1, m2 directly or indirectly use an attribute of class c in common. 
	 * 				  
	 * Input: List<Method>, List<Field>
	 * Output: int[k][k] where k is the number of methods which pass as input.
	*************************************************************************************************************/		
	

	public static int[][] getCommonAttributeUsageMatrix(List<CtMethod<?>> Methodlist,List<CtField<?>> Fieldlist){
		
		int[][] commonAttributeUsageMatrix=new int[Methodlist.size()][Methodlist.size()];
		
		for (int i = 0; i < Methodlist.size(); i++) 
			for (int j = i+1; j < Methodlist.size(); j++) 
				if (getCommonAttributeUsage(Methodlist.get(i),Methodlist.get(j),Fieldlist)){
					commonAttributeUsageMatrix[i][j]=1;
					commonAttributeUsageMatrix[j][i]=1;
				}
		//System.out.println(Arrays.deepToString(commonAttributeUsageMatrix));
		return commonAttributeUsageMatrix;
	}
	
	/*/**********************************************************************************************************
	 * Description : getCommonAttributeUsage() 
	 * 				cau(m1,m2)(common attribute usage) is true, if m1, m2 belong to MI(c) directly or indirectly use 
	 * 				an implement attribute of class c in common. 
	 * 				Based on "A Unified Framework for Cohesion Measurement in Object-Oriented Systems" page 77
	 * 
	 * Input: Method, Method, List<Field>
	 * Output: true means m1, amd m2 directly or indirectly use an attribute of class c in common.
	*************************************************************************************************************/	
	

	protected static boolean getCommonAttributeUsage(CtMethod<?> method1,CtMethod<?> method2,List<CtField<?>> Fieldlist){
		
		Set<CtField<?>> FieldList1 = new HashSet<CtField<?>>(); /**all attributes which are referenced by method1 directly or indirectly*/
		List<CtMethod<?>> simm1_all=new ArrayList<CtMethod<?>>();  /**SIM*(method1)*/	
		simm1_all=allInvokedMethods(method1);
		simm1_all.add(method1);
		for (CtMethod<?> m : simm1_all) 
			for (CtFieldReference<?> field : getAttributeReferences(m)) 
				if(Fieldlist.contains(field.getDeclaration())) 
					FieldList1.add( field.getDeclaration());
					
		Set<CtField<?>> FieldList2 = new HashSet<CtField<?>>(); /**all attributes which are referenced by method2 directly or indirectly*/
		List<CtMethod<?>> simm2_all=new ArrayList<CtMethod<?>>();  /**SIM*(method2)*/
		simm2_all=allInvokedMethods(method2);
		simm2_all.add(method2);
		for (CtMethod<?> m : simm2_all) 
			for (CtFieldReference<?> field : getAttributeReferences(m)) 
				if(Fieldlist.contains(field.getDeclaration()))
					FieldList2.add(field.getDeclaration());
		for (CtField<?> field : FieldList1) 
			if (FieldList2.contains(field)) /**Attribute is share between two methods*/
				return true;
		
		return false;
	}
	
	   /*/**********************************************************************************************************
     * Description : getStarCommonAttributeUsageMatrix() cau
     * 				 The matrix is k*k where k is the number of methods which pass as input.
     * 				 A[i,j]=1 if cau*(i,j)==1. In fact, each room contains cau* for that array room.				 
     * 				 cau*(m1,m2)(common attribute usage) is true, if [cau(m1,m2)==true] or [cau(m1,m')==true and cau(m',m2)==true] 
     * 				  
     * Input: List<Method>, List<Field>
     * Output: int[k][k] where k is the number of methods which pass as input.
    *************************************************************************************************************/
public static int[][] getStarCommonAttributeUsageMatrix(List<CtMethod<?>> Methodlist,List<CtField<?>> Fieldlist){
    		
    		int[][] commonAttributeUsageMatrix=getCommonAttributeUsageMatrix(Methodlist,Fieldlist);
    		int [][] starCommonAttributeUsageMatrix=new int[Methodlist.size()][Methodlist.size()];
    		
    		for (int i = 0; i < commonAttributeUsageMatrix.length; i++) 
    			for (int j = 0; j < commonAttributeUsageMatrix[0].length; j++) 
    				if (commonAttributeUsageMatrix[i][j]==1){
    					starCommonAttributeUsageMatrix[i][j]=1;
    					for (int k = 0; k < starCommonAttributeUsageMatrix[j].length; k++) 
    						if ((i!=k)&&(commonAttributeUsageMatrix[j][k]==1)){
    							starCommonAttributeUsageMatrix[i][k]=1;
    							starCommonAttributeUsageMatrix[k][i]=1;
    						}
    				}		
    		
    		return starCommonAttributeUsageMatrix;
    	}
			
/*/**********************************************************************************************************
 * Description : getTransitiveImplementAttributeReferences() 
 * 				 This methods returns only implement attribute which are referenced by "method", it also contain
 * 				 attributes which are reference by methods that are referenced by "method" transitive attribute reference.
 *  
 * Input: Method, ClassDeclaration (method is implement there) 
 * Output: TransitiveImplementAttributeReferences
************************************************************************************************************/


public static Set<CtField<?>> getTransitiveImplementAttributeReferences(CtMethod<?> method, List<CtMethod<?>> MethodList, CtClass<?> cd){
		
		Set<CtField<?>> TransitiveImplementAttributeReferences = new HashSet<CtField<?>>();
			
		for (CtField<?> field : getTransitiveAttributeReferences(method,MethodList)) 
			if ((field!=null)&&field.getDeclaringType().getQualifiedName().equals(cd.getQualifiedName()))
				TransitiveImplementAttributeReferences.add(field);
		return TransitiveImplementAttributeReferences;
	}
/*/**********************************************************************************************************
 * Description : getTransitiveAttributeReferences() 
 * 				 TAR(m): for each m belongs to M(c) let TAR(m) be the set of attributes referenced by method m.
 * 				 T means "Transitive closure of attributes". Whenever Mi invokes method Mj, Mi transitively uses 
 * 				 each attribute being used by Mj. 
 * 			
 * 				 This methods returns all attributes which are defined in class "cd" (include inherited attributes) 
 * 				 and are referenced by "method". 
 * 				
 * 				 Based on "A SENSITIVE METRIC OF CLASS COHESION(SCOM)" page 88
 *  
 * Input: Method
 * Output: TAR(m), it doesn't contain duplicate value
*************************************************************************************************************/	
	
protected static List<CtField<?>> getTransitiveAttributeReferences(CtMethod<?> method, List<CtMethod<?>> MethodsList) {
	
		List<CtField<?>> transtiveAttributeReferencelist = new ArrayList<CtField<?>>();		
		/**First: attributes which are called directly*/
		List<CtFieldReference<?>> refs =getAttributeReferences(method);
		for(CtFieldReference<?> ref:refs) {
		 transtiveAttributeReferencelist.add(ref.getDeclaration());}
	
		/**Second: Transitive attribute References*/
		CtType<?> declaringType = method.getDeclaringType();
		List <CtExecutableReference<?>> exeref= method.filterChildren(new TypeFilter<CtInvocation<?>>(CtInvocation.class)).map((CtInvocation<?> inv) -> inv.getExecutable()).list();
		for(CtExecutableReference<?> exe: exeref){ 
			CtExecutable<?> executable = exe.getDeclaration();
			CtMethod<?> Invokedmethod = (CtMethod<?>) executable; 
			if((Invokedmethod!=null)&& (Invokedmethod.getDeclaringType().getQualifiedName().equals(declaringType.getQualifiedName())) 
 			  					  &&(MethodsList.contains(Invokedmethod)))
			{ List<CtFieldReference<?>> refs2 =getAttributeReferences(Invokedmethod);
 	  			for(CtFieldReference<?> ref:refs2) {	 
 	  				transtiveAttributeReferencelist.add(ref.getDeclaration());
 	  			}
			}
 	 
   }
	return transtiveAttributeReferencelist;
}
    	
public static int[][] getParameterOccuranceMatrix(List<CtMethod<?>> methodlist, List<CtTypeReference<?>> parameterset){
	
	int[][] parameterOccuranceMatric=new int[methodlist.size()][parameterset.size()];
	
	for (int i = 0; i < methodlist.size(); i++) {
		Set<CtTypeReference<?>> tempparameterset = getParameterSet(methodlist.get(i));
		for (int j = 0; j < parameterset.size(); j++) 
			if (tempparameterset.contains(parameterset.get(j)))
				parameterOccuranceMatric[i][j]=1;
	}
	
	return parameterOccuranceMatric;
}    	
    	
public static ArrayList<CtTypeReference<?>> getParameterSet(List<CtMethod<?>> methodlist){
	
	ArrayList<CtTypeReference<?>> parameterset = new ArrayList<CtTypeReference<?>>();
	for (CtMethod<?> method : methodlist) 
		for (CtTypeReference<?> type : getParameterSet(method)) 
			if (!parameterset.contains(type)) 
				//if (!(type.getQualifiedName().equals(method.getDeclaringType().toString()))) /**implicit "this" parameter*/
				if (!type.equals(method.getDeclaringType())) /**implicit "this" parameter*/
					parameterset.add(type);

	return parameterset;
}


private static Set<CtTypeReference<?>> getParameterSet(CtMethod<?> method){
    
	Set<CtTypeReference<?>> parameterset = new HashSet<CtTypeReference<?>>();
	for (CtParameter<?> parameter : method.getParameters()){ 
		Set<CtTypeReference<?>> type =  parameter.getReferencedTypes();
			parameterset.addAll(type); 
	}
	
	return parameterset;
}
   	
/*/**********************************************************************************************************
* Description : getMethodMethodInvocationCohesion()
* 				 The cohesion is the average number of methodï¿½method-invocation interactions. This is represented by 
* 				 the ratio of the number of 1s in the MI matrix to the total size of the matrix. However, recursive method 
* 				 invocations should be excluded because we are interested in measuring the cohesion between pairs of different methods.
*  			 Based on "An object-oriented high-level design-base class cohesion metric page 1352".
*  
* Input: List<Method>
* Output:returns Method-Method Invocation Cohesion (MMIC)value. 
************************************************************************************************************/	 
	public static double getMethodMethodInvocationCohesion(List<CtMethod<?>> Methodlist,CtModel model){
			
		double summation = 0.0;
		int[][]  MethodInnovacationMatrix = getMethodInnovacationMatrix(Methodlist,model);
				
		/**if k=0 or k=1 :return 0*/
		if ((MethodInnovacationMatrix.length == 0)||(MethodInnovacationMatrix.length == 1))
			return 0;
		
		int k = MethodInnovacationMatrix.length;	
		for (int i = 0; i < k; i++)
			for (int j = 0; j < k; j++)
				if (i != j) summation += MethodInnovacationMatrix[i][j];
				
		double MMIC = summation / (k * (k - 1));
			
		return MMIC;
	}
	  
    /*/**********************************************************************************************************
     * Description : getMethodMethodThroughAtributesCohesion()
     * 				 The MMAC is the average cohesion of all pairs of methods.
     *  			 It uses "AT" matrix contains "k" methods(row) and "l" distinct attribute types (col).
     *  			 Based on "An object-oriented high-level design-base class cohesion metric page 1352".
     *  
     * Input: List<Method>, List<Field>
     * Output:returns Method-Method Through Attributes Cohesion (MMAC) value. 
    ************************************************************************************************************/	 
    	public static double getMethodMethodThroughAtributesCohesion(List<CtMethod<?>> Methodlist,List<CtField<?>> Fieldlist,CtModel model){
    		
    		double summation = 0.0;
    		int[][] AttributeTypeMatrix = getAttributeTypeMatrix(Methodlist, Fieldlist,model);
    		
    		/**if k=0 or l=0 :return 0*/
    		if ((AttributeTypeMatrix.length == 0)||(AttributeTypeMatrix[0].length == 0))
    			return 0;
    		
    		/**if k=1 :return 1*/
    		if (AttributeTypeMatrix.length == 1)
    			return 1;
    		
    		int k = AttributeTypeMatrix.length;
    		
    		int l = AttributeTypeMatrix[0].length;
    		
    		for (int i = 0, count = 0; i < l; i++, count = 0){ 
    			for (int j = 0; j < k; j++)
    				count += AttributeTypeMatrix[j][i];
    			
    			count*=(count-1);
    			summation+=count;
    		}
    		//System.out.println(summation);
    		double MMAC=summation/(l*k*(k-1));
    		
    		return MMAC;
    	} 	
	
   	 /*/**********************************************************************************************************
         * Description : getAttributeTypeMatrix() 
         * 				 The Attribute Type matrix (AT) has the same rows and columns as "DirectAttributeTypeMatrix", 
         * 				 but it differs in that it models both direct and transitive interactions.				 
         * 				 A value of 1 in the matrix indicates that the attribute type matches the parameter 
         * 				 type of the method or the methods that are directly or transitively invoked by the method.
         * 				 Based on "An object-oriented high-level design-base class cohesion metric page 1351"
         * 
         * 				 Excluding or including inherited methods as well as attributes depends on algorithm. 
         * 				 methods and fields is passed as methods's parameter. So, depends on algorithm it contains or no contains inherited elements.
         * 				 Exclude constructors 				 
         * Input: List<Method>, List<Field>
         * Output:returns Attribute Type (AT) Matrix. 
        ************************************************************************************************************/	 
        	private static int[][] getAttributeTypeMatrix(List<CtMethod<?>> methods, List<CtField<?>> fields,CtModel model){
        		
        		int[][]  DirectAttributeTypeMatrix = getDirectAttributeTypeMatrix(methods, fields);
        		
        		int[][]  MetodInvocationMatrix = getMethodInnovacationMatrix(methods,model);
        		int[][]	 AttributeTypeMatrix = getDirectAttributeTypeMatrix(methods, fields);
        
        		boolean flag = true;
        		int row = methods.size();
        		while (flag){
        			flag = false;
        			for (int i = 0; i < row; i++) 
        				for (int j = 0; j < row; j++) 
        					if((MetodInvocationMatrix[i][j] == 1) && (i != j))
        						for (int k = 0; k < AttributeTypeMatrix[i].length; k++) 
        							if (DirectAttributeTypeMatrix[j][k] == 1)
        								if (AttributeTypeMatrix[i][k] != 1){
        									AttributeTypeMatrix[i][k] = 1;
        									flag = true;
        								}
        		}
        		
        		return AttributeTypeMatrix;
        	}
        	
        	
       	 /*/**********************************************************************************************************
            * Description : getDirectAttributeTypeMatrix() 
            * 				 The Direct Attribute Matrix is a binary "k*l" matrix, where "k" is the number of 
            * 				 methods and "l" is the number of distinct attribute types in the class of interest.
            * 				 The matrix is based on the assumption that the set of attribute types accessed by a 
            * 				 method is the intersection of the set of this method's parameter types and the set 
            * 				 of its class attribute types.
            * 				 DAT[i,j]=1 if jth attribute type is a type of at least one of the parameters or return of the ith method. 
            * 				 Based on "An object-oriented high-level design-base class cohesion metric page 1350"
            * 
            * 				 Excluding or including inherited methods as well as attributes depends on algorithm. 
            * 				 Methodlist and Fieldlist is passed as methods's parameter.So, depends on algorithm it contains or no contains inherited elements.
            * 				 Exclude constructors 				 
            * Input: List<Method>, List<Field>
            * Output:return Direct Attribute Type (DAT) Matrix. 
           ************************************************************************************************************/	 
           	private static int[][] getDirectAttributeTypeMatrix(List<CtMethod<?>> Methodlist,List<CtField<?>> Fieldlist){
           					
           		Set<CtTypeReference<?>> fieldTypeSet=new HashSet<CtTypeReference<?>>();
           		for (CtField<?> field : Fieldlist) 
           			fieldTypeSet.add(field.getType());
           		List<CtTypeReference<?>> fieldTypeList = new ArrayList<CtTypeReference<?>>(fieldTypeSet);
           		
           		int[][] DirectAttributeTypeMatrix=new int[Methodlist.size()][fieldTypeList.size()];
           		
           		for (int i = 0; i < Methodlist.size(); i++) 
           			for (CtTypeReference<?> methodType : getMethodTypeSet(Methodlist.get(i))) 
           				if (fieldTypeList.contains(methodType))
           					DirectAttributeTypeMatrix[i][fieldTypeList.indexOf(methodType)]=1;
           			
           		return DirectAttributeTypeMatrix;
           	}
      	  /*/**********************************************************************************************************
             * Description : getMethodTypeSet() 
             *  			 The method returns the type of "input parameters" as well as the type of "return statement".
             * Input: Method
             * Output: returns MethodTypeSet for a method, no duplicate
            *************************************************************************************************************/
            	private static Set<CtTypeReference<?>> getMethodTypeSet(CtMethod<?> method){
            				
            		Set<CtTypeReference<?>> MethodTypeSet = new HashSet<CtTypeReference<?>>();
            		List<CtParameter<?>> parameters=method.getParameters();
            		for (CtParameter<?> parameter : parameters){
            			Set<CtTypeReference<?>> type =  parameter.getReferencedTypes();
            			MethodTypeSet.addAll(type);
            		}
            		MethodTypeSet.add(method.getType());
            	
            		return MethodTypeSet;
            	}    	

       	
       	
 	           
                /*/**********************************************************************************************************
                 * Description : getAtributeAtributeCohesion()
                 * 				 The AAC is the average cohesion of all pairs of attributes.
                 *  			 It uses "AT" matrix contains "k" methods(row) and "l" distinct attribute types (col).
                 *  			 Based on "An object-oriented high-level design-base class cohesion metric page 1352".
                 *  
                 * Input: List<Method>, List<Field>
                 * Output:returns Attribute-Attribute Cohesion (AAC) value. 
                ************************************************************************************************************/	 
                	public static double getAttributeAttributeCohesion(List<CtMethod<?>> Methodlist, List<CtField<?>> Fieldlist, CtModel model){
                			
                		double summation=0.0;
                		int[][] AttributeTypeMatrix = getAttributeTypeMatrix(Methodlist,Fieldlist,model);
                					
                		/**if k=0 or l=0 :return 0*/
                		if ((AttributeTypeMatrix.length==0)||(AttributeTypeMatrix[0].length==0))
                			return 0;
                		/**if l=1 :return 1*/
                		if (AttributeTypeMatrix[0].length==1)
                			return 1;
                			
                		int k=AttributeTypeMatrix.length;
                		int l=AttributeTypeMatrix[0].length;
                		for (int i = 0, count=0; i < k; i++,count=0){ 
                			for (int j = 0; j < l; j++)
                				count+=AttributeTypeMatrix[i][j];
                				
                			count*=(count-1);
                			summation+=count;
                		}
                		double AAC=summation/(l*k*(l-1));
                			
                		return AAC;
                	}

               	 /*/**********************************************************************************************************
                     * Description : getAttributeMethodCohesion()
                     * 				 The AMC is the ratio of the number of 1s in the AT matrix to the total size of the matrix.
                     *  			 It uses "AT" matrix contains "k" methods(row) and "l" distinct attribute types (col).
                     *  			 Based on "An object-oriented high-level design-base class cohesion metric page 1352".
                     *  
                     * Input: List<Method>, List<Field>
                     * Output:returns Attribute-Method Cohesion (AMC) value. 
                    ************************************************************************************************************/	 
                    	public static double getAttributeMethodCohesion(List<CtMethod<?>> Methodlist,List<CtField<?>> Fieldlist, CtModel model){
                    		
                    		double summation=0.0;
                    		int[][] AttributeTypeMatrix = getAttributeTypeMatrix(Methodlist,Fieldlist,model);
                    		
                    		/**if k=0 or l=0 :return 0*/
                    		if ((AttributeTypeMatrix.length==0)||(AttributeTypeMatrix[0].length==0))
                    			return 0;
                    		
                    		int k=AttributeTypeMatrix.length;
                    		int l=AttributeTypeMatrix[0].length;
                    		for (int i = 0; i < k; i++)
                    			for (int j = 0; j < l; j++)
                    				summation+=AttributeTypeMatrix[i][j];
                    		
                    		double AMC=summation/(k*l);
                    		
                    		return AMC;
                    	}

                        /*/**********************************************************************************************************
                         * Description : getParameterOccuranceMatrix() 
                         *  
                         * Input: ClassDeclaration
                         * Output: 
                        *************************************************************************************************************/	 
                        	public static int[][] getParameterOccuranceMatrix(List<CtMethod<?>> methodlist, ArrayList<CtTypeReference<?>> parameterset){
                        		
                        		int[][] parameterOccuranceMatric=new int[methodlist.size()][parameterset.size()];
                        		
                        		for (int i = 0; i < methodlist.size(); i++) {
                        			Set<CtTypeReference<?>> tempparameterset = getParameterSet(methodlist.get(i));
                        			for (int j = 0; j < parameterset.size(); j++) 
                        				if (tempparameterset.contains(parameterset.get(j)))
                        					parameterOccuranceMatric[i][j]=1;
                        		}
                        		
                        		return parameterOccuranceMatric;
                        	}

            	




}
