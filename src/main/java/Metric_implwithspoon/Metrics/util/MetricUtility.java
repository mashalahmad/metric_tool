package Metric_implwithspoon.Metrics.util;

import java.util.ArrayList;
import java.util.List;

import spoon.reflect.code.CtInvocation;
import spoon.reflect.declaration.CtExecutable;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.declaration.CtType;
import spoon.reflect.reference.CtExecutableReference;
import spoon.reflect.reference.CtFieldReference;
import spoon.reflect.reference.CtTypeReference;
import spoon.reflect.visitor.filter.TypeFilter;

public class MetricUtility {
	
	/**   Metric Utility  ****/

	private static List<CtMethod<?>> indirectlyInvokedMethods1level(CtMethod<?> method){ 
		
		List<CtMethod<?>>  indirectInvokeMethodSet = new ArrayList<CtMethod<?>>(); 
			
		for (CtMethod<?> InvokedMethod : staticallyInvokedMethods(method)) {
			for (CtMethod<?> m : staticallyInvokedMethods(InvokedMethod)) 
				if (!method.equals(m)) 
					indirectInvokeMethodSet.add(m);
		}
	//System.out.println(indirectInvokeMethodSet);
		return indirectInvokeMethodSet;
	}



	private static   List<CtMethod<?>> staticallyInvokedMethods(CtMethod<?> method){
		   
	    List<CtMethod<?>> staticallyInvokedMethodSet =new ArrayList<CtMethod<?>>();
	    CtType<?> declaringType = method.getDeclaringType();
		  
	    List <CtExecutableReference<?>> exeref= method.filterChildren(new TypeFilter(CtInvocation.class)).map((CtInvocation<?> inv) -> inv.getExecutable()).list();
	   for(CtExecutableReference<?> exe: exeref){ 
	 	  CtExecutable<?> executable = exe.getDeclaration();
	 	  CtMethod<?> method2 = (CtMethod<?>) executable;
	 	  if((method2!=null)&&(method2.getDeclaringType().getQualifiedName().equals(declaringType.getQualifiedName())))
	 		  staticallyInvokedMethodSet.add(method2);
	    }
	   
	return staticallyInvokedMethodSet;	       
	}



	public static List<CtMethod<?>> allInvokedMethods(CtMethod<?> method){
		
		/* All method which m calls directly or indirectly.*/
		List<CtMethod<?>> AllInvokeMethodSet = new ArrayList<CtMethod<?>>(); 
			
		AllInvokeMethodSet = staticallyInvokedMethods(method);
		
		for (CtMethod<?> m : indirectlyInvokedMethods1level(method)) 
					AllInvokeMethodSet.add(m);
			
		//System.out.println(AllInvokeMethodSet);
		return AllInvokeMethodSet;
	}

	/*/**********************************************************************************************************
	 * Description : referenceAtLeastOneAttribute() 
	 *				 If method reference at least on attribute in its body returns true.
	 * 
	 * Input:   Method
	 * Output : True means an attribute is accessed by method. 
	*************************************************************************************************************/


	public static boolean referenceAtLeastOneAttribute(CtMethod<?> method){
		
		CtType<?> declaringType = method.getDeclaringType();
		List<CtFieldReference<?>> fields=method.filterChildren(new TypeFilter<>(CtFieldReference.class))
												.map((CtFieldReference<?> fieldRef)-> fieldRef.getDeclaration())
												.select((CtFieldReference<?> fieldRef)-> fieldRef.getDeclaringType().getQualifiedName().equals(declaringType.getQualifiedName()))
												.list();
		
		for(CtFieldReference<?> field: fields) {
			CtTypeReference<?> ct = field.getDeclaringType();
			if(ct.getDeclaringType().getQualifiedName().equals(declaringType.getQualifiedName()))
				return true;
		}
		
		return false;
	}
	
	
	
	
}
