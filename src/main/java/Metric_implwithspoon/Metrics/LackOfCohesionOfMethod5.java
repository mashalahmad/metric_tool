package Metric_implwithspoon.Metrics;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import Metric_implwithspoon.Metrics.util.CohesionUtility;
import spoon.reflect.CtModel;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtMethod;
import Metric_implwithspoon.Metrics.MetricNotDefinedForSourceException;

public class LackOfCohesionOfMethod5 extends CohesionUtility{

	 public static double calculateLCOM5( CtModel model ) throws MetricNotDefinedForSourceException {
		 
		 List<CtField<?>> getfields=getFields(model);
			List<CtMethod<?>> getmethods=getMethods(model);
			
			 int k = getmethods.size();
		     int l = getfields.size();
		 
		     Set<CtField<?>> ImplementAttributeReferences = new HashSet<CtField<?>>(); 
		     double Count = 0.0, LCOM5 = 0.0;
		     /** If k = 0, cohesion is not defined.*/
		        if (k == 0)
		        	System.out.println("Metric Undefined");
		            
		        /** If l = 0 and k > 1, return 1.*/
		        if ((l == 0) && (k > 1))
		                 return 1;

		        /** If k = 1,: returns 0 - not sure about this.*/
		        if(k == 1)
		        	     return 0;

		        for (CtField<?> a : getfields) 
		            for (CtMethod<?> m : getmethods){ 
		            	CtClass<?> cd = m.getParent(CtClass.class);
		                ImplementAttributeReferences = getImplementAttributeReferences(m,cd);
		                if (ImplementAttributeReferences.contains(a))
		                    Count++;
		            }
		            
		        LCOM5 = k - ((1.0 / l) * Count);
		        LCOM5 /= (k - 1.0);
		            
		          return LCOM5;
		 
	 }


}
