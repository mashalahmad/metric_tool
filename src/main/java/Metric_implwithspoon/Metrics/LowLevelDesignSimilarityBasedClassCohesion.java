package Metric_implwithspoon.Metrics;

import java.util.List;

import Metric_implwithspoon.Metrics.util.CohesionUtility;
import spoon.reflect.CtModel;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtMethod;
import Metric_implwithspoon.Metrics.MetricNotDefinedForSourceException;

public class LowLevelDesignSimilarityBasedClassCohesion extends CohesionUtility {
	 
public static double calculateLscc( CtModel model ) throws MetricNotDefinedForSourceException  {
	 double result=0;
			List<CtField<?>> fieldslist=getFields(model);
			List<CtMethod<?>> methodslist=getMethods(model);
		// System.out.println(fieldslist);
	     int k = getMethods(model).size();
	     int l = getFields(model).size();
	     /** If k = 0 and l = 0, the cohesion is not defined. */
	        if (!((k == 0) && (l == 0))) {
	           
	            if ((l == 0) && (k > 1)) {
	                /** If l = 0 and k > 1, return 0. */
	                result = 0.0;
	            } else if ((k == 1) || ((l > 0) && (k == 0))) {
	                /** If k = 1 or k = 0 and l > 0, return 1. */
	                result = 1.0;
	            } else {
	    
	                int[][] MethodAttributeReferenceMatrix = getMethodAttributeReferenceMatrix(methodslist,fieldslist,model);
	                double sum = 0.0;
	    
	                for (int i = 0, count = 0; i < l; i++, count = 0) {
	                    for (int j = 0; j < k; j++)
	                        count += MethodAttributeReferenceMatrix[j][i];
	    
	                    count *= (count - 1);
	                    sum += count;
	                }
	                result = sum / (l * k * (k - 1));
	            }
	            return result;
	        } else {
	        	return result=0;
	        }
		 
	 }
	
	

}
