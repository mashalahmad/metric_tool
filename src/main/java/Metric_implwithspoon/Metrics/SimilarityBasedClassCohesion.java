package Metric_implwithspoon.Metrics;

import Metric_implwithspoon.Metrics.util.CohesionUtility;
import spoon.reflect.CtModel;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.reference.CtTypeReference;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import Metric_implwithspoon.Metrics.MetricNotDefinedForSourceException;

public class SimilarityBasedClassCohesion extends CohesionUtility{
	
	
	 public static double calculateSCC( CtModel model ) throws MetricNotDefinedForSourceException {
		 List<CtField<?>> getfields=getFields(model);
			List<CtMethod<?>> getmethods=getMethods(model);
			
			int k = getmethods.size();
			
			Set<CtTypeReference<?>> fieldTypeSet = new HashSet<CtTypeReference<?>>();
	        for (CtField<?> field : getfields)
	           fieldTypeSet.add(field.getType());
	        
	        int l = fieldTypeSet.size();
	        
	        /** If k = 0 and l = 0, the cohesion is not defined. */
	        if (!((k == 0) && (l == 0))) {

	            double result = 0.0;

	            if ((k == 0) && (l > 0)) {
	                /** If k = 0 and l > 0, return 0. */
	                result = 0.0;
	            } else if ((k == 1) && (l == 0)) {
	                /** If k = 1 and l = 0, return 1. */
	                result = 1.0;
	            } else if ((k > 1) && (l == 0)) {
	                /** If k > 1 and l = 0, return MMIC. */
	                result = getMethodMethodInvocationCohesion(getmethods,model);
	                
	            } else {
	                /** Otherwise */
	                double MMAC = getMethodMethodThroughAtributesCohesion(getmethods, getfields,model); 
	               //System.out.println(MMAC);
	                double MMIC = getMethodMethodInvocationCohesion(getmethods,model);
	                //System.out.println(MMIC);
	                double AAC = getAttributeAttributeCohesion(getmethods,getfields,model);
	                
	                double AMC = getAttributeMethodCohesion(getmethods,getfields,model); 
	                
	                double temp1 = k * (k - 1);
	                temp1 *= (MMAC + (2 * MMIC));

	                double temp2 = l * (l - 1);
	                temp2 *= AAC;
	                temp2 += 2 * l * k * AMC;

	                temp1 += temp2;

	                temp2 = 3 * k * (k - 1);
	                temp2 += (l * (l - 1));
	                temp2 += 2 * l * k;

	                result = (temp1 / temp2);
	            }
	            return result;
	        } else {
	        	 throw new MetricNotDefinedForSourceException();
	        }
			
		 
		 
	 }

}
