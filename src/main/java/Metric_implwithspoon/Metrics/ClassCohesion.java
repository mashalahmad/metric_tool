package Metric_implwithspoon.Metrics;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import Metric_implwithspoon.Metrics.util.CohesionUtility;
import spoon.reflect.CtModel;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtMethod;
import Metric_implwithspoon.Metrics.MetricNotDefinedForSourceException;
public class ClassCohesion extends CohesionUtility {
	
	

	 public static double calculateCC( CtModel model ) throws MetricNotDefinedForSourceException {
		 		 
		 double similarity = 0.0;
	     double Intersection, UnionSize;
	     Map<CtMethod<?>, Set<CtField<?>>> mp = new HashMap<CtMethod<?>, Set<CtField<?>>>();
	     List<CtMethod<?>> methodslist = getMethods(model);
	     List<CtField<?>> fieldlist = getFields(model);
	       int k = methodslist.size();
	       int l = fieldlist.size();
	     
	     for(CtMethod<?> method: methodslist){		
			CtClass<?> cd = method.getParent(CtClass.class);	
			mp.put(method, getImplementAttributeReferences(method,cd)); }
			/**
	         * A class with only "one method" or "no method" is considered to be
	         * highly cohesive and hence assigned a "CC" value of "1".
	         */
	        if ((k == 0) || (k == 1))
	            return 1;
	        	
	        	
	        /**
	         * A class with "no instance variable" is considered to be highly
	         * cohesive and hence assigned a "CC" value of "1".
	         */
	        if (l == 0)
	            return 1;
			
	        
	       // System.out.println(mp.get(methodslist.get(4)));
			for (int i = 0; i < k - 1; i++) {
	            Set<CtField<?>> fieldlist1 = mp.get(methodslist.get(i));
	            if(fieldlist1!=null)
	            for (int j = i + 1; j < k; j++) {
	                Intersection = 0;
	                UnionSize = 0;
	                Set<CtField<?>> fieldlist2 = mp.get(methodslist.get(j));
	                if(fieldlist2!=null)
	                for (CtField<?> field : fieldlist2)
	                    if (fieldlist1.contains(field))
	                        Intersection++;
	                if(fieldlist2!=null)
	                for (CtField<?> field : fieldlist2)
	                    if (!fieldlist1.contains(field))
	                        UnionSize++;

	                UnionSize += fieldlist1.size();
	                if (UnionSize != 0)
	                    similarity += ((Intersection) / (UnionSize));
	            }
			} 
			
		     
			double down = (k * (k - 1)) / 2.0;

			double CC=similarity / down;  
			
			return CC;
		 
		 
	 }

	

}
