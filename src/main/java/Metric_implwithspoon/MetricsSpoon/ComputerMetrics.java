package Metric_implwithspoon.MetricsSpoon;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import Metric_implwithspoon.Metrics.ClassCohesion;
import Metric_implwithspoon.Metrics.CohesionAmongMethods;
import Metric_implwithspoon.Metrics.LackOfCohesionOfMethod5;
import Metric_implwithspoon.Metrics.LowLevelDesignSimilarityBasedClassCohesion;
import Metric_implwithspoon.Metrics.MetricNotDefinedForSourceException;
import spoon.IncrementalLauncher;
import spoon.reflect.CtModel;

public class ComputerMetrics {
	
	public static void main(String[] args) throws IOException, MetricNotDefinedForSourceException  {
		
		
		String fileName =  "src/main/resources/Full_data_before/Metric_computation.csv"; 
		FileWriter fileWriter = new FileWriter(fileName,true);
		BufferedWriter out = new BufferedWriter(fileWriter);
		File directory2 = new File("src/main/resources/Full_data_before/");
		
		Set<String> source= new HashSet<String>();	
		Set<File> inputsources = new HashSet<File>();
		//inputsources.add(directory2);
		
		 try (Stream<Path> paths = Files.walk(Paths.get(directory2.toString()))) {					
  			 paths.skip(1)
  			 	 .forEach(s->{
  			 		source.add(s.toString());		  			 	
  			 	 });
		 } catch (IOException e) {
			    e.printStackTrace();
			}   	
		// System.out.println(source);
		 
		for(Iterator<String> it=source.iterator(); it.hasNext();) {	 
		 IncrementalLauncher launcher = new IncrementalLauncher(inputsources, source, directory2, false); 
	     String newfile = it.next().toString();
		 launcher.addInputResource(newfile); 
	     System.out.println(newfile);
		CtModel model=launcher.buildModel();
		//model.setBuildModelIsFinished(false);
		double lcom5=LackOfCohesionOfMethod5.calculateLCOM5(model);
		double cc=ClassCohesion.calculateCC(model); 
		double Lscc= LowLevelDesignSimilarityBasedClassCohesion.calculateLscc(model);
		double camc= CohesionAmongMethods.calculateCAMC(model);
		System.out.println("Class Cohesion ="+cc);
		System.out.println("Low level Class Cohesion ="+Lscc);
		System.out.println("LCOM5  = "+lcom5);
		System.out.println("camc="+camc);
		    	
			
			 String regex = "(Jan?|Feb?|Mar?|Apr?|May|Jun?|Jul?|Aug?|Sep?|Oct?|Nov?|Dec?)\\s+\\d{1,2},\\s+\\d{4}";
			    Pattern pattern = Pattern.compile(regex);
			    Matcher m = pattern.matcher(newfile);
			    if (m.find()) {
			        String date= m.group(0);
			        out.write("\""+newfile+"\"");
			        out.write(",");
			    	out.write("\""+date+"\"");
					out.write(",");
					out.write(String.valueOf(lcom5));
					out.write(",");
					out.write(String.valueOf(cc));
					out.write(",");
					out.write(String.valueOf(Lscc));
					out.write(",");
					out.write(String.valueOf(camc));
					
					out.write("\n"); 
			       }
			
			
		}
		
		out.close();
	}

}
