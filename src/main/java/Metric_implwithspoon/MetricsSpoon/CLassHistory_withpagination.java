package Metric_implwithspoon.MetricsSpoon;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CLassHistory_withpagination {
	
	
	
	public static void main(String[] args) throws IOException  {
		   
		   
		final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36";	        
		
		String M_URL = "https://github.com/real-logic/aeron/commits/master/aeron-cluster/src/main/java/io/aeron/cluster/RecordingCatchUp.java";
		
		String directory="real-logic/aeron";
		
		String ending ="aeron-cluster/src/main/java/io/aeron/cluster/RecordingCatchUp.java";
		
		String Filename1="C:/Users/UCD/Desktop/Reports-senttoMel/After_stage_transfer/Evolution/Non-recipient/RecordingCatchUp/";
		
		Document document2 = Jsoup.connect(M_URL).userAgent(USER_AGENT).timeout(0).get();				
	 		
		Elements firstcommitlinks = document2.select("li.commit");
	
	    for(Element firstcommitlink : firstcommitlinks) { 
	    	Elements Commitlinks    =	firstcommitlink.select("a.sha");
			String Commitlink		 =	Commitlinks.attr("href");
			Element TimenDate =    firstcommitlink.getElementsByTag("relative-time").first();
			
			Commitlink=Commitlink.split("#diff")[0];
			Commitlink=Commitlink.split("commit/")[1];	
	    	
	    	String Url= "https://raw.githubusercontent.com/"+directory+"/"+Commitlink+"/"+ending;
	    		 	
	    	/*************************Download the file**********************************/
	    	String fileExtension = ".java";
		    String fileName2 = Commitlink+TimenDate.text()+fileExtension;
		    
		    try { 
		    
			URL website = new URL(Url);
			ReadableByteChannel rbc = Channels.newChannel(website.openStream());		
			//saving file to specific directory
			FileOutputStream fos = new FileOutputStream(Filename1+fileName2);
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
			fos.close();
			
		    }catch (Exception ignore) {
        		  // ignoring the Exception because file link doesn't exist
  	           }
	    	  
	    }
		

	    
	   if(!document2.select("div.pagination a").isEmpty()) {
	    String pagelink1 = document2.select("div.pagination a").get(0).attr("href");
	    
		 //an int just to count up links
	    int i = 1;
		  
	    Document document3 = Jsoup.connect(pagelink1).userAgent(USER_AGENT).timeout(0).get();
	    Elements commitlinks2 = document3.select("li.commit");
	  //  System.out.println(commitlinks2);
	    for(Element commitlink2 : commitlinks2) { 
	    	Elements Commitlinks    =	commitlink2.select("a.sha");
			String Commitlink		 =	Commitlinks.attr("href");
			Element TimenDate =    commitlink2.getElementsByTag("relative-time").first();
			//System.out.println(TimenDate);
			Commitlink=Commitlink.split("#diff")[0];
			Commitlink=Commitlink.split("commit/")[1];	
	    	
			String Url= "https://raw.githubusercontent.com/"+directory+"/"+Commitlink+"/"+ending;
	    	
	    	
	    	/*************************Download the file**********************************/
	    	String fileExtension = ".java";
		    String fileName2 = Commitlink+TimenDate.text()+fileExtension;
		   
		    try { 
		    
			URL website = new URL(Url);
			ReadableByteChannel rbc = Channels.newChannel(website.openStream());		
			//saving file to specific directory
			FileOutputStream fos = new FileOutputStream(Filename1+fileName2);
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
			fos.close();
			
		    }catch (Exception ignore) {
        		  // ignoring the Exception because file link doesn't exist
  	           }
	    	
	    	
	    	
	    }
	    	    
	    //parse next page using link
	    //check if the div on next page has more than one link in it
	    while(Jsoup.connect(pagelink1).get().select("div.pagination a").size() >1){
	    	pagelink1 = Jsoup.connect(pagelink1).get().select("div.pagination a").get(1).attr("href");
	     //   System.out.println("pagination-link_"+ (++i) +"\t" + pagelink1);
	        Document document4 = Jsoup.connect(pagelink1).userAgent(USER_AGENT).timeout(0).get();
		    Elements commitlinks3 = document4.select("li.commit");
		//    System.out.println(commitlinks3);
	        
		    for(Element commitlink3 : commitlinks3) { 
		    	
		    	Elements Commitlinks    =	commitlink3.select("a.sha");
				String Commitlink		 =	Commitlinks.attr("href");
				Element TimenDate =    commitlink3.getElementsByTag("relative-time").first();
				//System.out.println(TimenDate);
				Commitlink=Commitlink.split("#diff")[0];
				Commitlink=Commitlink.split("commit/")[1];	
		    	
				String Url= "https://raw.githubusercontent.com/"+directory+"/"+Commitlink+"/"+ending;
		    	
		    	
		    	/*************************Download the file**********************************/
		    	String fileExtension = ".java";
			    String fileName2 = Commitlink+TimenDate.text()+fileExtension;
			   
			    try { 
			    
				URL website = new URL(Url);
				ReadableByteChannel rbc = Channels.newChannel(website.openStream());		
				//saving file to specific directory
				FileOutputStream fos = new FileOutputStream(Filename1+fileName2);
				fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
				fos.close();
				
			    }catch (Exception ignore) {
	        		  // ignoring the Exception because file link doesn't exist
	  	           }
		    	
		    }
	        
	        
	    }
		 
		 
	}
	
	
	}
	
	
	
	

}
