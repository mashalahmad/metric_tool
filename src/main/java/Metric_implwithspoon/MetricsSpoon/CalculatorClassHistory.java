package Metric_implwithspoon.MetricsSpoon;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Stream;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import Metric_implwithspoon.Metrics.ClassCohesion;
import spoon.IncrementalLauncher;
import spoon.Launcher;
import spoon.reflect.CtModel;

public class CalculatorClassHistory  extends ClassCohesion {
	
	public static void main(String[] args) throws IOException  {
	
		 		   
			String fileName =  "links_with_datesPlayer.csv"; 
			FileWriter fileWriter = new FileWriter(fileName,true);
			BufferedWriter out = new BufferedWriter(fileWriter);
		//	out.write(String.format("Link", "Date", "CC"));

		   
		   String M_URL = "https://github.com/tinyMediaManager/tinyMediaManager/commits/master/src/main/java/org/tinymediamanager/ui/components/StatusBar.java";
		   List<String> Urls=  new ArrayList<String>();
		   
		   Document document2 = Jsoup.connect(M_URL).get();
		   Elements commits = document2.select("li.commit");
		   Elements e2=document2.select("span.disabled");
		   
		   // means no pagination else will iterate over the pagination
			 if(!e2.isEmpty()) { 
				 				
					//System.out.println(TimenDate);
				for(Element commit:commits ){ 
					
					Elements Commitlinks    =	commit.select("a.sha");
					String Commitlink		 =	Commitlinks.attr("href");
					Element TimenDate =    commit.getElementsByTag("relative-time").get(0);
					
					Commitlink=Commitlink.split("#diff")[0];
					Commitlink=Commitlink.split("commit/")[1];	
					String Url= "https://raw.githubusercontent.com/tinyMediaManager/tinyMediaManager/"+Commitlink+"/src/main/java/org/tinymediamanager/ui/components/StatusBar.java";
					Urls.add(Url);
					
					out.write(Url);
					out.write(",");
					out.write("\""+TimenDate.text()+"\"");
					out.write("\n"); 
					
					String fileExtension = ".java";
				    String fileName2 = Commitlink+TimenDate.text()+fileExtension;
				   
				    try { 
				    
					URL website = new URL(Url);
					ReadableByteChannel rbc = Channels.newChannel(website.openStream());
				
					//saving file to specific directory
					FileOutputStream fos = new FileOutputStream("C:/Users/UCD/eclipse-workspace/MetricsSpoon/Fulldataset(11-12-2018)/E39/"+fileName2);
					fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
					fos.close();
					
				    }catch (Exception ignore) {
		        		  // ignoring the Exception because file link doesn't exist
		  	           }
					
				 }
				
				
				out.close();
			 }
				
					
		}

	

}
