/**
 *  C-Nery for Android - A home automation web application for C-Bus.
 *  Copyright (C) 2012  Dave Oxley <dave@daveoxley.co.uk>.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.daveoxley.cnery.android;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import java.util.ArrayList;
import org.ksoap2.serialization.SoapObject;

/**
 *
 * @author dave
 */
public class CNeryPreferencesListener implements Runnable {

    private final static Object CREATE_MUTEX = new Object();
    private static CNeryPreferencesListener instance;
    private final Object MUTEX = new Object();
    private final Context context;
    private SharedPreferences prefs = null;
    private Thread thread = null;

    private static final String PROX_ALERT_INTENT = "com.daveoxley.cnery.android.ProximityAlert";
    private static final long POINT_RADIUS = 200; // in Meters
    private static final long PROX_ALERT_EXPIRATION = -1;

    private String username = null;
    private String password = null;
    private String url = null;
    private boolean demoMode = false;
    private String project = null;
    private String network = null;
    private String application = null;
    private boolean useAsGPSSensor = false;
    private PendingIntent proximityIntent = null;
    private ProximityIntentReceiver proximityIntentRec = null;
    private ArrayList projects;
    private ArrayList networks;
    private ArrayList applications;
    private ArrayList groups;

    static CNeryPreferencesListener initialise(Context context) {
        synchronized (CREATE_MUTEX) {
            if (instance == null) {
                instance = new CNeryPreferencesListener(context);
                CREATE_MUTEX.notifyAll();
            }
            return instance;
        }
    }

    static CNeryPreferencesListener getInstance() {
        synchronized (CREATE_MUTEX) {
            while (instance == null) {
                try {
                    CREATE_MUTEX.wait();
                }
                catch (InterruptedException ie) {
                    Thread.currentThread().interrupt();
                }
            }
            return instance;
        }
    }

    private CNeryPreferencesListener(final Context context) {
        this.context = context;

        // Initialise preferences with default values in a background thread
        // To prevent 'Can't create handler inside thread that has not called Looper.prepare()'
        // exception on some non-stock Android phones use a handler to setDefaultValues. Using
        // AsyncTask maybe enough to have fixed this but I cannot reproduce this issue on stock.
        // http://stackoverflow.com/questions/4187960/asynctask-and-looper-prepare-error
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                PreferenceManager.setDefaultValues(context, R.xml.preferences, false);
            }
        });

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        // Start thread to collect valid preference values
        start(prefs);
    }

    static String getSharedPrefsName(Context context) {
        return context.getPackageName() + "_preferences";
    }

    ArrayList getProjects() {
        synchronized (MUTEX) {
            while (this.thread != null) {
                try {
                    MUTEX.wait();
                }
                catch (InterruptedException ie) {
                    Thread.currentThread().interrupt();
                }
            }
            return projects;
        }
    }

    ArrayList getNetworks() {
        synchronized (MUTEX) {
            while (this.thread != null) {
                try {
                    MUTEX.wait();
                }
                catch (InterruptedException ie) {
                    Thread.currentThread().interrupt();
                }
            }
            return networks;
        }
    }

    ArrayList getApplications() {
        synchronized (MUTEX) {
            while (this.thread != null) {
                try {
                    MUTEX.wait();
                }
                catch (InterruptedException ie) {
                    Thread.currentThread().interrupt();
                }
            }
            return applications;
        }
    }

    ArrayList getGroups() {
        synchronized (MUTEX) {
            while (this.thread != null) {
                try {
                    MUTEX.wait();
                }
                catch (InterruptedException ie) {
                    Thread.currentThread().interrupt();
                }
            }
            return groups;
        }
    }

    private void start(SharedPreferences prefs) {
        synchronized (MUTEX) {
            this.prefs = prefs;
            if (thread == null) {
                thread = new Thread(this);
                thread.setDaemon(true);
                thread.start();
            }
        }
    }

    void onSharedPreferenceChanged(Context context, SharedPreferences prefs, String key) {
        if (key.equals("username") || key.equals("password") || key.equals("url") || key.equals("demoMode") ||
                key.equals("project") || key.equals("network") || key.equals("application") || key.equals("useAsGPSSensor")) {
            if (!key.equals("useAsGPSSensor")) {
                Editor edit = prefs.edit();
                if (key.equals("username") || key.equals("password") || key.equals("url") ||
                        key.equals("demoMode") || key.equals("project") || key.equals("network")) {
                    if (key.equals("username") || key.equals("password") || key.equals("url") || key.equals("demoMode") || key.equals("project")) {
                        if (key.equals("username") || key.equals("password") || key.equals("url") || key.equals("demoMode"))
                            edit.remove("project");
                        edit.remove("network");
                    }
                    edit.remove("application");
                }
                edit.remove("widgetGroup");
                edit.commit();
            }
            start(prefs);
        }
    }

    public void run() {
        boolean continueRunning;
        do {
            SharedPreferences prefs0;
            synchronized (MUTEX) {
                prefs0 = this.prefs;
                this.prefs = null;
            }

            boolean authChanged = (prefs0.contains("username") && !prefs0.getString("username", "").equals(username)) ||
                                  (prefs0.contains("password") && !prefs0.getString("password", "").equals(password)) ||
                                  (prefs0.contains("url") && !prefs0.getString("url", "").equals(url)) ||
                                  prefs0.getBoolean("demoMode", false) != demoMode;
            boolean projectChanged = prefs0.contains("project") && !prefs0.getString("project", "").equals(project);
            boolean networkChanged = prefs0.contains("network") && !prefs0.getString("network", "-1").equals(network);
            boolean applicationChanged = prefs0.contains("application") && !prefs0.getString("application", "-1").equals(application);
            boolean gpsSensorChanged = prefs0.getBoolean("useAsGPSSensor", false) != useAsGPSSensor;
            username = prefs0.getString("username", null);
            password = prefs0.getString("password", null);
            url = prefs0.getString("url", null);
            demoMode = prefs0.getBoolean("demoMode", false);
            project = prefs0.getString("project", null);
            network = prefs0.getString("network", null);
            application = prefs0.getString("application", null);
            useAsGPSSensor = prefs0.getBoolean("useAsGPSSensor", false);

            if (authChanged)
                projects = getProjects(prefs0);
            if (projects != null) {
                if (authChanged || projectChanged)
                    networks = getNetworks(prefs0);
                if (networks != null) {
                    if (authChanged || projectChanged || networkChanged)
                        applications = getApplications(prefs0);
                    if (applications != null) {
                        if (authChanged || projectChanged || networkChanged || applicationChanged)
                            groups = getGroups(prefs0);
                    }
                    else
                        groups = null;
                }
                else {
                    applications = null;
                    groups = null;
                }
            }
            else {
                networks = null;
                applications = null;
                groups = null;
            }

            if ((authChanged || gpsSensorChanged) && useAsGPSSensor) {
                SoapObject request = new SoapObject(CNeryWSClient.NAMESPACE, "getGPSHomeLocation");
                ArrayList<String> gpsLoc = CNeryWSClient.callCNeryWS(prefs0, "GPSSensor", request);
                if (gpsLoc != null && gpsLoc.size() >= 2) {
                    try {
                        double latitude = Double.parseDouble(gpsLoc.get(0));
                        double longitude = Double.parseDouble(gpsLoc.get(1));

                        LocationManager locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);

                        if (proximityIntent != null)
                            locationManager.removeProximityAlert(proximityIntent);
                        proximityIntent = null;

                        Intent intent = new Intent(PROX_ALERT_INTENT);
                        proximityIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

                        locationManager.addProximityAlert(
                                    latitude, // the latitude of the central point of the alert region
                                    longitude, // the longitude of the central point of the alert region
                                    POINT_RADIUS, // the radius of the central point of the alert region, in meters
                                    PROX_ALERT_EXPIRATION, // time for this proximity alert, in milliseconds, or -1 to indicate no expiration 
                                    proximityIntent // will be used to generate an Intent to fire when entry to or exit from the alert region is detected
                            );

                        if (proximityIntentRec == null) {
                            IntentFilter filter = new IntentFilter(PROX_ALERT_INTENT);
                            proximityIntentRec = new ProximityIntentReceiver();
                            context.registerReceiver(proximityIntentRec, filter);
                        }
                        else
                                proximityIntentRec.reset();
                    }
                    catch (NumberFormatException nfe) {
                        Log.e("CNeryPreferencesListener", "C-Nery getGPSHomeLocation", nfe);
                        //Toast.makeText(context.getApplicationContext(), nfe.getClass().getName() + " " + nfe.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            } else if (gpsSensorChanged && !useAsGPSSensor && proximityIntent != null) {
                LocationManager locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
                locationManager.removeProximityAlert(proximityIntent);
                proximityIntent = null;
            }

            synchronized (MUTEX) {
                continueRunning = this.context != null && this.prefs != null;
                if (!continueRunning) {
                    this.thread = null;
                    MUTEX.notifyAll();
                }
            }
        } while (continueRunning);
    }

    private ArrayList getProjects(SharedPreferences prefs) {
        SoapObject request = new SoapObject(CNeryWSClient.NAMESPACE, "listProjects");
        return CNeryWSClient.callCNeryWS(prefs, "Admin", request);
    }

    private ArrayList getNetworks(SharedPreferences prefs) {
        if (!prefs.contains("project") || prefs.getString("project", "").equals(""))
            return null;

        SoapObject request = new SoapObject(CNeryWSClient.NAMESPACE, "listNetworks");
        request.addProperty("project", prefs.getString("project", ""));
        return CNeryWSClient.callCNeryWS(prefs, "Project", request);
    }

    private ArrayList getApplications(SharedPreferences prefs) {
        if (!prefs.contains("project") || prefs.getString("project", "").equals("") ||
                !prefs.contains("network") || prefs.getString("network", "-1").equals(""))
            return null;

        SoapObject request = new SoapObject(CNeryWSClient.NAMESPACE, "listApplications");
        request.addProperty("project", prefs.getString("project", ""));
        int network;
        try {
            network = Integer.parseInt(prefs.getString("network", "-1"));
        }
        catch (NumberFormatException nfe) {
            network = -1;
            Log.e("CNeryPreferencesListener", "Getting network from prefs", nfe);
            //Toast.makeText(context.getApplicationContext(), nfe.getClass().getName() + " " + nfe.getMessage(), Toast.LENGTH_LONG).show();
        }
        request.addProperty("network", network);
        return CNeryWSClient.callCNeryWS(prefs, "Network", request);
    }

    private ArrayList getGroups(SharedPreferences prefs) {
        if (!prefs.contains("project") || prefs.getString("project", "").equals("") ||
                !prefs.contains("network") || prefs.getString("network", "-1").equals("") ||
                !prefs.contains("application") || prefs.getString("application", "-1").equals(""))
            return null;

        SoapObject request = new SoapObject(CNeryWSClient.NAMESPACE, "listGroups");
        request.addProperty("project", prefs.getString("project", ""));
        int network;
        try {
            network = Integer.parseInt(prefs.getString("network", "-1"));
        }
        catch (NumberFormatException nfe) {
            network = -1;
            Log.e("CNeryPreferencesListener", "Getting network from prefs", nfe);
            //Toast.makeText(context.getApplicationContext(), nfe.getClass().getName() + " " + nfe.getMessage(), Toast.LENGTH_LONG).show();
        }
        request.addProperty("network", network);
        int application;
        try {
            application = Integer.parseInt(prefs.getString("application", "-1"));
        }
        catch (NumberFormatException nfe) {
            application = -1;
            Log.e("CNeryPreferencesListener", "Getting application from prefs", nfe);
            //Toast.makeText(context.getApplicationContext(), nfe.getClass().getName() + " " + nfe.getMessage(), Toast.LENGTH_LONG).show();
        }
        request.addProperty("application", application);
        return CNeryWSClient.callCNeryWS(prefs, "Application", request);
    }
}
