package shrine.sensorreader;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Get Sensor Events, extract critical information, format it (e.g. for easier gnuplot parsing or
 * efficient wire footprint) and write to output stream
 */
public class SensorReader implements SensorEventListener {
    protected static final String fileName = "output.csv";
    /**
     * dataSize defines the size of the newData buffer
     */
    private final int dataSize = 256;
    /**
     * A buffer for raw sensor data. If the buffer is full, its content is written to an output
     * file and the buffer gets overwritten.
     */
    protected float[] newData;
    /** dataCount counts the amount of data in the newData buffer. */
    private int dataCount;
    /**
     * sampleCounter is counting the overall amount of raw sensor samples. It is needed to plot the
     * data with GNUPlot
     */
    private int sampleCounter;
    /**
     * Output stream where the formatted data is being sent to
     */
    private OutputStreamWriter writer;
    private SensorManager mSensorManager;
    Context context;

    public SensorReader(OutputStreamWriter w, SensorManager sm, Context c) throws IOException {
        newData = new float[3 * dataSize]; // room for dataSize x,y,z tuples
        dataCount = 0;
        sampleCounter = 0;
        writer = w;
        mSensorManager = sm;
        context = c;
        // Write the file header
        writer.write(getDeviceInfo() + "# sample" + SensorReader.padRight("| x", 14)
                + SensorReader.padRight("| y", 16) + SensorReader.padRight("| z", 16) + "\n"
                + "#-----------------------------------------------------\n");
    }

    /* Source for this function: http://www.rgagnon.com/javadetails/java-0448.html
     */
    public static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);
    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Nothing to do here
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        newData[3 * dataCount] = event.values[0];
        newData[3 * dataCount + 1] = event.values[1];
        newData[3 * dataCount + 2] = event.values[2];
        dataCount++;
        /* if dataCount >= dataSize it means that the newData buffer is full.  In this case its
         * content gets written into the output file.
         */
        if (dataCount >= dataSize) {
            Log.d(MainActivity.TAG, "dataCount = " + dataCount + " >= " + dataSize + " = dataSize");
            try {
                writer.write(dataToString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            dataCount = 0;
        }
    }

    private String dataToString() {
        String result = "";
        for (int i = 0; i < dataCount; i++) {
            result += padRight(sampleCounter++ + ", ", 8) + padRight("" + newData[3 * i], 14)
                    + padRight(", " + newData[3 * i + 1], 16)
                    + padRight(", " + newData[3 * i + 2], 16) + "\n";
        }
        return result;
    }

    protected void saveRemainingData() throws IOException {
        writer.write(dataToString());
    }

    private String getDeviceInfo() {
        Log.d(MainActivity.TAG, "getting some device info...");
        String deviceInfo = "# Device Information: \n";

        if (null == mSensorManager) {
            mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        }

        // Get device name
        deviceInfo += "# Manufacturer:" + Build.MANUFACTURER + "\n";
        deviceInfo += "# Model: " + Build.MODEL + "\n";

        // get OS version
        deviceInfo += "# API-level: " + Build.VERSION.SDK_INT + "\n";
        // TODO: Maybe check if the device is rooted or not. See http://stackoverflow.com/questions/1101380/determine-if-running-on-a-rooted-device

        // get available sensors
        Sensor accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        deviceInfo += gatherInfo(accelerometer, "Accelerometer");

        Sensor gravitySensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        deviceInfo += gatherInfo(gravitySensor, "Gravity Sensor");

        Sensor gyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        deviceInfo += gatherInfo(gyroscope, "Gyroscope");

        Sensor rotationSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        deviceInfo += gatherInfo(rotationSensor, "Rotation Vector Sensor");

        return deviceInfo;
    }

    private String gatherInfo(Sensor s, String n) {
        String result;
        if (null == s) {
            result = "# This device does not have a " + n + "\n";
        }
        else {
            result = "# " + n + ": " + s.getName() + "\n";
            result += "#     Resolution: " + s.getResolution() + "\n";
            result += "#     Minimal Delay: " + s.getMinDelay() + "\n";
        }
        return result;
    }
}
