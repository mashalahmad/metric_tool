/*      Copyright (C) 2015  David Ross

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.example.davidross.djr1_reflex;

import android.app.Activity;
import android.content.Context;

import java.io.FileInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class StatisticController {
    public static final String reflexTimeSave = "Reflex.sav";
    public final String twoPlayerSave = "twoPlayer.sav";
    public final String threePlayerSave = "threePlayer.sav";
    public final String fourPlayerSave = "fourPlayer.sav";

    private StatisticList reflexStats;
    private StatisticList twoPlayerStats;
    private StatisticList threePlayerStats;
    private StatisticList fourPlayerStats;

    private Context context;


    public StatisticController(Context parent){
        this.context = parent;

        this.reflexStats = load(reflexTimeSave);
        this.twoPlayerStats = load(twoPlayerSave);
        this.threePlayerStats = load(threePlayerSave);
        this.fourPlayerStats = load(fourPlayerSave);
    }


    public void save(Statistic stat, String filename) {
        try {
            reflexStats.addToStartStatistic(stat);
            FileOutputStream fos = context.openFileOutput(filename, context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(reflexStats);
            oos.close();
            fos.flush();
            fos.close();
        }
        catch(FileNotFoundException e){
            throw new RuntimeException();
        }catch(IOException e){
            throw new RuntimeException();
        }
    }


    public StatisticList load(String filename){
        try{
            FileInputStream fis = context.openFileInput(filename);
            ObjectInputStream ois = new ObjectInputStream(fis);
            StatisticList loaded = (StatisticList) ois.readObject();
            ois.close();
            fis.close();
            return loaded;
        }catch(FileNotFoundException e){
            return new StatisticList();
        }catch(IOException e) {
            throw new RuntimeException();
        } catch (ClassNotFoundException e) {
        }
        return new StatisticList();
    }

    public String formatStatistics(){
        StatisticsCalculator calc = new StatisticsCalculator(reflexStats);
        String stats = "Minimum Time out of all tries:  "+ calc.getMinimumTime(reflexStats.size())+" \n" +
                       "Maximum time out of all tries   "+ calc.getMaximumTime(reflexStats.size())+"\n";
        return stats;

    }
}
