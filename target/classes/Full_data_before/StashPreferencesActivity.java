package com.geekeclectic.android.stashcache;

import android.app.ActionBar;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

/**
 * Activity for the preferences screen (since attempting to use a preference fragment was not compatible
 * with the support fragment manager required for viewpager).  Reruns the shopping list after a
 * change in preferences to update it in case the user has selected full skein usage in kitting patterns.
 */
public class StashPreferencesActivity extends PreferenceActivity {

    public static final String KEY_NEW_SKEIN_FOR_EACH = "new_skein_for_each";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
